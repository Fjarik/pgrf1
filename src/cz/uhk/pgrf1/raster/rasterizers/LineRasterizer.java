package cz.uhk.pgrf1.raster.rasterizers;

import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.models.graphics.GraphicsObject;
import cz.uhk.pgrf1.models.graphics.Line;
import cz.uhk.pgrf1.raster.Raster;
import transforms.Point2D;

import java.awt.*;
import java.util.List;

public abstract class LineRasterizer {
    protected final Raster raster;

    protected LineRasterizer(Raster raster) {
        this.raster = raster;
    }

    public void rasterize(GraphicsObject obj) {
        if (obj instanceof Line) {
            rasterize((Line) obj);
            return;
        }
        rasterize(obj.getLines());
    }

    public void rasterize(Line line) {
        rasterize(line.getStart(), line.getEnd(), line.getColour());
    }

    public void rasterize(List<Line> lines) {
        for (Line line : lines) {
            rasterize(line);
        }
    }

    public void rasterize(Point2D start, Point2D end, Color color) {
        rasterize(new Point(start), new Point(end), color);
    }

    public void rasterize(Point start, Point end, Color color) {
        if (start == null)
            start = end; // Start je null → jeden bod - end

        if (end == null)
            end = start; // End je null → jeden bod - start

        if (end == null)
            return; // End je stále null → žádný bod

        rasterize(start.getX(), start.getY(), end.getX(), end.getY(), color);
    }

    public abstract void rasterize(int x1, int y1, int x2, int y2, Color color);

}
