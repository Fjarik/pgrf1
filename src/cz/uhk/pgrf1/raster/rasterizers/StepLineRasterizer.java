package cz.uhk.pgrf1.raster.rasterizers;

import cz.uhk.pgrf1.raster.Raster;

import java.awt.*;

public abstract class StepLineRasterizer extends LineRasterizer {
    protected final int step;

    protected StepLineRasterizer(Raster raster, int step) {
        super(raster);
        this.step = step;
    }

    /**
     * Tato funkce používá upravený Triviální algoritmus pro rasterizaci úsečky.
     * Nevýhody:
     * Násobení floatů
     * Fungování ve všech kvadrantech vyžaduje úpravdu
     * Výhody:
     * Pochopitelný
     * Jednoduché debuggování
     */
    @Override
    public void rasterize(int x1, int y1, int x2, int y2, Color color) {
        int dx = x2 - x1;
        int dy = y2 - y1;

        if (dx == dy && dx == 0) {
            // dx a dy = 0 → jen jeden bod
            raster.setPixel(x1, y1, color);
            return;
        }

        // y = k*x + q
        // x = (y-q)/k

        int absDx = Math.abs(dx);
        float k = absDx > 0 ? ((float) dy / dx) : 0; // k = (y2-y1)/(x2-x1)

        float q = y1 - (x1 * k); // q = y - (k*x)

        // if (k > 1 || k < -1 || x1 == x2) {
        if (Math.abs(dy) < absDx) {
            int start = Math.min(x1, x2);
            int end = Math.max(x1, x2);

            for (int i = start; i <= end; i += step) {
                int y = Math.round(k * i + q);
                raster.setPixel(i, y, color);
            }
        } else {
            int start = Math.min(y1, y2);
            int end = Math.max(y1, y2);

            for (int i = start; i < end; i += step) {
                int x = x1;
                if (k != 0) { // Zabránění dělení nulou
                    x = Math.round((i - q) / k);
                }
                raster.setPixel(x, i, color);
            }
        }
    }
}
