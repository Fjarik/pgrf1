package cz.uhk.pgrf1.raster.rasterizers;

import cz.uhk.pgrf1.raster.Raster;

public class DottedLineRasterizer extends StepLineRasterizer {

    /**
     * Implementace je unvnitř 'StepLineRasterizer' líší se pouze podle krokování ('step')
     */


    public DottedLineRasterizer(Raster raster, int step) {
        super(raster, step);
    }

}
