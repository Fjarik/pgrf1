package cz.uhk.pgrf1.raster.rasterizers;

import cz.uhk.pgrf1.raster.Raster;

public class FilledLineRasterizer extends StepLineRasterizer {

    /**
     * Implementace je unvnitř 'StepLineRasterizer' líší se pouze podle krokování ('step')
     */

    public FilledLineRasterizer(Raster raster) {
        super(raster, 1);
    }

}
