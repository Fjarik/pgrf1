package cz.uhk.pgrf1.raster.rasterizers;

import cz.uhk.pgrf1.raster.Raster;
import cz.uhk.pgrf1.raster.RasterBufferedImage;

import java.awt.*;
import java.awt.image.BufferedImage;

public class LineRasterizerGraphics extends LineRasterizer {

    public LineRasterizerGraphics(Raster raster) {
        super(raster);
    }

    @Override
    public void rasterize(int x1, int y1, int x2, int y2, Color color) {
        BufferedImage img = ((RasterBufferedImage) raster).getImg();
        Graphics g = img.getGraphics();
        g.setColor(color);
        g.drawLine(x1, y1, x2, y2);
    }
}
