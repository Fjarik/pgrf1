package cz.uhk.pgrf1.raster;

import cz.uhk.pgrf1.models.Point;

import java.awt.*;
import java.awt.image.BufferedImage;

public class RasterBufferedImage implements Raster {

    private final BufferedImage img;
    private final Color backgroundColour;

    public RasterBufferedImage(int width, int height, Color backgroundColor) {
        this.img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        this.backgroundColour = backgroundColor;
        clear();
    }

    public BufferedImage getImg() {
        return img;
    }

    @Override
    public int getWidth() {
        return img.getWidth();
    }

    @Override
    public int getHeight() {
        return img.getHeight();
    }

    @Override
    public Color getBackgroundColour() {
        return backgroundColour;
    }

    @Override
    public void setPixel(Point p, Color color) {
        setPixel(p.getX(), p.getY(), color);
    }

    @Override
    public void setPixel(int x, int y, Color color) {
        if (isInvalid(x, y)) {
            return; // Neplatné souřadnice
        }
        img.setRGB(x, y, color.getRGB());
    }

    @Override
    public Color getPixel(Point point) {
        return getPixel(point.getX(), point.getY());
    }

    @Override
    public Color getPixel(int x, int y) {
        if (isInvalid(x, y)) {
            return getBackgroundColour(); // Neplatné souřadnice
        }
        return new Color(img.getRGB(x, y));
    }

    private boolean isInvalid(int x, int y) {
        return x < 0 || y < 0 || x >= getWidth() || y >= getHeight();
    }

    @Override
    public void clear() {
        Graphics gr = img.getGraphics();
        gr.setColor(backgroundColour);
        gr.fillRect(0, 0, getWidth(), getHeight());
    }

    public void present(RasterBufferedImage raster) {
        present(raster.getImg().getGraphics());
    }

    @Override
    public void present(Graphics graphics) {
        graphics.drawImage(img, 0, 0, null);
    }

}
