package cz.uhk.pgrf1.raster.fill.scanline;

import cz.uhk.pgrf1.helpers.Sorter;
import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.raster.Raster;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Vlastní implementace scan-line filler
 * (Průsečíky počítám už v předpracování)
 */
public class ScanLineFiller extends ScanLineFillerBase {

    protected Map<Integer, List<Integer>> intersections;

    public ScanLineFiller(Raster raster) {
        super(raster);
    }

    @Override
    protected void setObject(List<Point> points, FillColor fillColour) {
        this.fillColour = fillColour;
        this.intersections = new HashMap<>();
        int yMin = Integer.MAX_VALUE;
        int yMax = Integer.MIN_VALUE;

        if (points.size() < 3) {
            return;
        }

        ArrayList<BorderLine> borderLineList = new ArrayList<>();

        for (int i = 0; i < points.size(); i++) {
            Point current = points.get(i);

            int index = (i + 1) % points.size();
            Point next = points.get(index);

            BorderLine line = new BorderLine(current, next);
            if (!line.isHorizontal) {
                borderLineList.add(line);

                if (line.minY < yMin)
                    yMin = line.minY;

                if (yMax < line.maxY)
                    yMax = line.maxY;
            }
        }

        for (int y = yMin; y <= yMax; y++) {
            List<Integer> yIntersections = new ArrayList<>();
            for (BorderLine line : borderLineList) {
                if (line.hasIntersection(y)) {
                    yIntersections.add(line.getIntersection(y));
                }
            }
            intersections.put(y, yIntersections);
        }

        for (Map.Entry<Integer, List<Integer>> entry : intersections.entrySet()) {
            Sorter.insertionSort(entry.getValue());
        }
        // intersections.forEach((k, v) -> v.sort(Comparator.comparingInt(Point::getX)));

    }

    @Override
    public void fill() {
        if (fillColour == null || intersections == null)
            return;

        intersections.forEach((y, yIntersections) -> {
            for (int i = 0; i < yIntersections.size() - 1; i += 2) {
                int x1 = yIntersections.get(i);
                int x2 = yIntersections.get(i + 1);
                fillLine(y, x1, x2);
            }
        });

    }
}
