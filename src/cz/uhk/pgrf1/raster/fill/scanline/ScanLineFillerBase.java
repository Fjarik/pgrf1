package cz.uhk.pgrf1.raster.fill.scanline;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.models.graphics.GraphicsPointedObject;
import cz.uhk.pgrf1.raster.Raster;
import cz.uhk.pgrf1.raster.fill.Filler;

import java.util.List;

public abstract class ScanLineFillerBase extends Filler {

    protected FillColor fillColour;

    public ScanLineFillerBase(Raster raster) {
        super(raster);
    }

    public void setObject(GraphicsPointedObject obj) {
        if (obj == null)
            return;

        setObject(obj.getPoints(), obj.getFillColour());
    }

    protected abstract void setObject(List<Point> points, FillColor fillColour);

    @Override
    public abstract void fill();

    protected void fillLine(int y, int x1, int x2) {
        for (int x = x1; x <= x2; x++) {
            raster.setPixel(x, y, fillColour.getColour(x, y));
        }
    }

    protected static class BorderLine {

        private final Point start;
        private final Point end;
        public final boolean isHorizontal;
        private final double k;
        private final double q;
        public final int maxY;
        public final int minY;

        public BorderLine(Point start, Point end) {
            this.start = start.getY() > end.getY() ? end : start;
            this.end = start.getY() > end.getY() ? start : end;
            this.minY = this.start.getY();
            this.maxY = this.end.getY();
            this.isHorizontal = minY == maxY;
            this.k = calcK();
            this.q = calcQ();
        }

        public boolean hasIntersection(int y) {
            return y < maxY && y >= minY;
        }

        public int getIntersection(int y) {
            return (int) Math.round(k * y + q);
        }

        private double calcK() {
            int dx = end.getX() - start.getX();
            int dy = end.getY() - start.getY();
            return Math.abs(dy) > 0 ? ((float) dx / dy) : 0;
        }

        private double calcQ() {
            return start.getX() - (start.getY() * k);
        }

    }

}
