package cz.uhk.pgrf1.raster.fill.scanline;

import cz.uhk.pgrf1.helpers.Sorter;
import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.raster.Raster;

import java.util.ArrayList;
import java.util.List;

/**
 * ScanLine filler ze cvičení
 */
public class ScanLineFillerCv extends ScanLineFillerBase {

    protected List<BorderLine> borderLineList;
    private int yMin;
    private int yMax;

    public ScanLineFillerCv(Raster raster) {
        super(raster);
    }

    @Override
    protected void setObject(List<Point> points, FillColor fillColour) {
        this.fillColour = fillColour;
        this.borderLineList = new ArrayList<>();
        yMin = Integer.MAX_VALUE;
        yMax = Integer.MIN_VALUE;

        if (points.size() < 3) {
            return;
        }

        for (int i = 0; i < points.size(); i++) {
            Point current = points.get(i);

            int index = (i + 1) % points.size();
            Point next = points.get(index);

            BorderLine line = new BorderLine(current, next);
            if (!line.isHorizontal) {
                borderLineList.add(line);

                if (line.minY < yMin)
                    yMin = line.minY;

                if (yMax < line.maxY)
                    yMax = line.maxY;
            }
        }
    }

    @Override
    public void fill() {
        if (fillColour == null)
            return;

        /*
            máme seznam vrcholů
            nalezení yMin a yMax
            Ignorujeme vodorovné úsečky
            Otočení úseček, tak aby y1 > y2
            Zkrácení úseček o pixel
         */

        if (borderLineList.size() < 2) {
            return;
        }

        for (int y = yMin; y <= yMax; y++) {
            List<Integer> yIntersections = new ArrayList<>();
            // Najdi všechny průsečíky
            for (BorderLine line : borderLineList) {
                if (line.hasIntersection(y)) {
                    int intersectionX = line.getIntersection(y);
                    yIntersections.add(intersectionX);
                }
            }

            Sorter.bubbleSort(yIntersections);

            for (int i = 0; i < yIntersections.size() - 1; i += 2) {
                int x1 = yIntersections.get(i);
                int x2 = yIntersections.get(i + 1);
                fillLine(y, x1, x2);
            }
        }
    }
}
