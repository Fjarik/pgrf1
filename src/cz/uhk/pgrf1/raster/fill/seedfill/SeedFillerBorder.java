package cz.uhk.pgrf1.raster.fill.seedfill;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.raster.Raster;

import java.awt.*;

/**
 * Rekurzivní seed fill
 */
public class SeedFillerBorder extends SeedFillerBase {

    private final Color lineColour;
    private boolean[][] visitedPoints;

    public SeedFillerBorder(Raster raster, FillColor fillColour, Color lineColour) {
        super(raster, fillColour);
        this.lineColour = lineColour;
    }

    private void fill(int x, int y) {
        if (!isInside(x, y)) {
            return;
        }
        if (visitedPoints[x][y])
            return;

        visitedPoints[x][y] = true;
        raster.setPixel(x, y, fillColour.getColour(x, y));
        fill(x + 1, y);
        fill(x - 1, y);
        fill(x, y + 1);
        fill(x, y - 1);
    }

    @Override
    public void fill() {
        if (seed == null || lineColour == null)
            return;

        // V některých případech zbytečně velké, ale funguje ¯\_(°-°)_/¯
        this.visitedPoints = new boolean[raster.getWidth()][raster.getWidth()];
        fill(seed.getX(), seed.getY());
    }

    @Override
    protected boolean isInside(int x, int y) {
        if (x < 0 || y < 0)
            return false;

        if (x >= raster.getWidth() || y >= raster.getHeight())
            return false;

        Color current = raster.getPixel(x, y);
        return !current.equals(lineColour) && !current.equals(backgroundColour);
    }

}
