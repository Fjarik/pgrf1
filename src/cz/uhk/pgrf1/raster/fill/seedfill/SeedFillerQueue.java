package cz.uhk.pgrf1.raster.fill.seedfill;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.raster.Raster;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Nerekurzivní seed fill
 */
public class SeedFillerQueue extends SeedFillerBase {

    public SeedFillerQueue(Raster raster, FillColor fillColour) {
        super(raster, fillColour);
    }

    @Override
    public void fill() {
        if (seed == null)
            return;

        backgroundColour = raster.getPixel(seed);

        Queue<Point> queue = new LinkedList<>();
        queue.add(seed);

        while (!queue.isEmpty()) {
            Point p = queue.remove();
            // Šlo by použít .poll(),
            // ale díky podmínce cyklu si můžu být jistý, že vždy bude ve frontě alespoň jeden bod

            if (!isInside(p))
                continue;

            raster.setPixel(p, fillColour.getColour(p));

            queue.add(new Point(p).addX());
            queue.add(new Point(p).addX(-1));
            queue.add(new Point(p).addY());
            queue.add(new Point(p).addY(-1));
        }
    }

}
