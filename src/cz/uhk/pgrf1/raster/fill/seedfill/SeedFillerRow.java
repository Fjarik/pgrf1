package cz.uhk.pgrf1.raster.fill.seedfill;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.raster.Raster;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Pokus o nerekurzivní seed fill po řádku
 * Podle: http://www.math.tau.ac.il/~dcor/Graphics/cg-slides/flood-fill03.pdf
 * Ale moc nefunguje :(
 */
public class SeedFillerRow extends SeedFillerBase {

    public SeedFillerRow(Raster raster, FillColor fillColour) {
        super(raster, fillColour);
    }

    @Override
    public void fill() {
        if (seed == null)
            return;

        backgroundColour = raster.getPixel(seed);

        Queue<Point> queue = new LinkedList<>();
        queue.add(seed);

        while (!queue.isEmpty()) {
            Point p = queue.remove();

            boolean up = false;
            boolean down = false;

            int x = p.getX();
            int y = p.getY();

            while (isInside(--x, y)) {
                while (isInside(++x, y)) {
                    raster.setPixel(x, y, fillColour.getColour(x, y));

                    if (!up) {
                        if (isInside(x, y + 1)) {
                            queue.add(new Point(x, y + 1));
                            up = true;
                        }
                    } else if (!isInside(x, y + 1)) {
                        up = false;
                    }

                    if (!down) {
                        if (isInside(x, y - 1)) {
                            queue.add(new Point(x, y - 1));
                            down = true;
                        }
                    } else if (!isInside(x, y - 1)) {
                        down = false;
                    }

                }
            }


        }
    }
}
