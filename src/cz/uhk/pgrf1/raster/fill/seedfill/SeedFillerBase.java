package cz.uhk.pgrf1.raster.fill.seedfill;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.raster.Raster;
import cz.uhk.pgrf1.raster.fill.Filler;

import java.awt.*;

public abstract class SeedFillerBase extends Filler {

    protected Color backgroundColour;
    protected Point seed;
    protected final FillColor fillColour;

    protected SeedFillerBase(Raster raster, FillColor fillColour) {
        super(raster);
        this.fillColour = fillColour;
    }

    public void setSeed(Point seed) {
        this.seed = seed;
    }

    protected boolean isInside(Point p) {
        return isInside(p.getX(), p.getY());
    }

    protected boolean isInside(int x, int y) {
        if (x < 0 || y < 0)
            return false;

        if (x >= raster.getWidth() || y >= raster.getHeight())
            return false;

        return raster.getPixel(x, y).equals(backgroundColour);
    }

}
