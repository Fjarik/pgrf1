package cz.uhk.pgrf1.raster.fill.seedfill;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.raster.Raster;

/**
 * Rekurzivní seed fill
 */
public class SeedFiller extends SeedFillerBase {

    public SeedFiller(Raster raster, FillColor fillColour) {
        super(raster, fillColour);
    }

    private void fill(int x, int y) {
        if (!isInside(x, y)) {
            return;
        }
        raster.setPixel(x, y, fillColour.getColour(x, y));
        fill(x + 1, y);
        fill(x - 1, y);
        fill(x, y + 1);
        fill(x, y - 1);
    }

    @Override
    public void fill() {
        if (seed == null)
            return;

        backgroundColour = raster.getPixel(seed);
        fill(seed.getX(), seed.getY());
    }
}
