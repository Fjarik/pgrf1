package cz.uhk.pgrf1.raster.fill;

import cz.uhk.pgrf1.models.Point;

import java.awt.*;

public class Pattern {

    private final Color[][] colours;
    private final int height;
    private final int width;

    public Pattern(Color[][] colours) {
        if (colours == null || colours.length < 1) {
            this.colours = new Color[][]{
                    new Color[]{Color.PINK}
            };
        } else {
            this.colours = colours;
        }
        height = this.colours.length;
        width = this.colours[0].length;
    }

    public Color getColour(Point p) {
        return getColour(p.getX(), p.getY());
    }

    public Color getColour(int x, int y) {
        int i = x % width;
        int j = y % height;
        return colours[j][i];
    }

}
