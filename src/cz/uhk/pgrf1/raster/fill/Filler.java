package cz.uhk.pgrf1.raster.fill;

import cz.uhk.pgrf1.raster.Raster;


public abstract class Filler {

    protected final Raster raster;

    public Filler(Raster raster) {
        this.raster = raster;
    }

    public abstract void fill();
}
