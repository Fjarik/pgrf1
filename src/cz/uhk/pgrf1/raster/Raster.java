package cz.uhk.pgrf1.raster;


import cz.uhk.pgrf1.models.Point;

import java.awt.*;

public interface Raster {
    int getWidth();

    int getHeight();

    Color getBackgroundColour();

    void setPixel(Point p, Color color);

    void setPixel(int x, int y, Color color);

    Color getPixel(int x, int y);

    Color getPixel(Point point);

    void clear();

    void present(Graphics graphics);
}
