package cz.uhk.pgrf1.render;

import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.models3D.ColourBuffer;
import cz.uhk.pgrf1.models3D.Scene;
import cz.uhk.pgrf1.models3D.solids.Solid;
import cz.uhk.pgrf1.raster.Raster;
import cz.uhk.pgrf1.raster.rasterizers.LineRasterizer;
import transforms.Mat4;
import transforms.Point2D;
import transforms.Point3D;
import transforms.Vec3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class WireframeEngine extends Renderer {

    public WireframeEngine(Raster raster, LineRasterizer lineRasterizer) {
        super(raster, lineRasterizer);
    }

    @Override
    public void render(Solid solid, Mat4 viewTransformation, Mat4 projectionTransformation) {
        Mat4 trans = viewTransformation.mul(projectionTransformation);
        render(solid, trans);
    }

    @Override
    public void render(Solid solid, Mat4 viewAndProjection) {
        List<Point3D> vertex = solid.getVertexBuffer();
        List<Integer> indexes = solid.getIndexBuffer();
        ColourBuffer colours = solid.getColourBuffer();

        Mat4 trans = solid.getModelTransformation().mul(viewAndProjection);

        List<Point3D> tempVertexBuffer = new ArrayList<>(vertex.size());

        for (Point3D point : vertex) {
            tempVertexBuffer.add(point.mul(trans));
        }

        int edgeIndex = 0;
        for (int i = 0; i < indexes.size() - 1; i += 2) {
            int indexA = indexes.get(i);
            int indexB = indexes.get(i + 1);
            Point3D a = tempVertexBuffer.get(indexA);
            Point3D b = tempVertexBuffer.get(indexB);
            Color c = colours.get(edgeIndex, Color.YELLOW);

            renderEdge(a, b, c);
            edgeIndex++;
        }
    }

    @Override
    public void render(Scene scene) {
        Mat4 viewAndProjection = scene.getViewTrans().mul(scene.getProjectionTrans());
        scene.getSolids()
                .stream()
                .filter(Solid::isVisible)
                .forEach(x ->
                        render(x, viewAndProjection)
                );
    }

    public void renderEdge(Point3D a, Point3D b, Color color) {
        // clip
        if (!isValid(a)) return;
        if (!isValid(b)) return;

        // dehomog
        Optional<Vec3D> a3dOptional = a.dehomog();
        if (!a3dOptional.isPresent()) return;
        Vec3D a3d = a3dOptional.get();

        if (!isValid(a3d)) return;

        Optional<Vec3D> b3dOptional = b.dehomog();
        if (!b3dOptional.isPresent()) return;
        Vec3D b3d = b3dOptional.get();

        if (!isValid(b3d)) return;
        // 3D → 2D

        Point2D a2d = new Point2D(a3d.getX(), a3d.getY());
        Point2D b2d = new Point2D(b3d.getX(), b3d.getY());

        // Viewport
        Point viewPointA = getViewportPoint(a2d);
        Point viewPointB = getViewportPoint(b2d);

        // lineRasterizer.rasterize
        lineRasterizer.rasterize(viewPointA, viewPointB, color);
    }

    private boolean isValid(Point3D a) {
        return (-a.getW()) <= a.getX() && a.getX() <= a.getW() &&
                (-a.getW()) <= a.getY() && a.getY() <= a.getW() &&
                0 <= a.getZ() && a.getZ() <= a.getW();
    }

    private boolean isValid(Vec3D a) {
        return -1 <= a.getX() && a.getX() <= 1 &&
                -1 <= a.getY() && a.getY() <= 1 &&
                0 <= a.getZ() && a.getZ() <= 1;
    }

    private Point getViewportPoint(Point2D a2d) {
        double newX = (raster.getWidth() - 1) * (a2d.getX() + 1) / 2;
        double newY = (raster.getHeight() - 1) * (1 - a2d.getY()) / 2;
        return new Point(newX, newY);
    }
}
