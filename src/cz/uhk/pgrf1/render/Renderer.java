package cz.uhk.pgrf1.render;

import cz.uhk.pgrf1.models3D.Scene;
import cz.uhk.pgrf1.models3D.solids.Solid;
import cz.uhk.pgrf1.raster.Raster;
import cz.uhk.pgrf1.raster.rasterizers.LineRasterizer;
import transforms.Mat4;

public abstract class Renderer {
    protected final Raster raster;
    protected final LineRasterizer lineRasterizer;

    public Renderer(Raster raster, LineRasterizer lineRasterizer) {
        this.raster = raster;
        this.lineRasterizer = lineRasterizer;
    }

    public abstract void render(Solid solid, Mat4 viewTransformation, Mat4 projectionTransformation);

    public abstract void render(Solid solid, Mat4 viewAndProjection);

    public abstract void render(Scene scene);

}
