package cz.uhk.pgrf1.view;

import cz.uhk.pgrf1.models3D.SolidsTableModel;

import javax.swing.*;

public class SolidsDialog extends JDialog {
    private JTable table;

    public SolidsDialog(Panel3D panel) {
        super(SwingUtilities.getWindowAncestor(panel), "Solids");
        initGui();
        pack();
    }

    private void initGui() {
        table = new JTable();

        JScrollPane scrollPane = new JScrollPane(table);

        // scrollPane.setPreferredSize(new Dimension(100, 300));

        this.add(scrollPane);
    }

    public void setModel(SolidsTableModel model) {
        table.setModel(model);
      //  model.fireTableDataChanged();
    }

}
