package cz.uhk.pgrf1.view;

import javax.swing.*;

public class MainWindow extends JFrame {
    public MainWindow() {
        super("PGRF1 - Úkol č.1");
        initGui();
        pack();
    }

    private void initGui() {
        setTitle("UHK FIM PGRF : " + this.getClass().getName());
        setResizable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JTabbedPane tabbedPane = new JTabbedPane();

        JPanel twoDPanel = new Panel2D();
        tabbedPane.addTab("2D", twoDPanel);

        JPanel threeDPanel = new Panel3D();
        tabbedPane.addTab("3D", threeDPanel);

        tabbedPane.addChangeListener(x -> {
            int index = tabbedPane.getSelectedIndex();
            JComponent c = (JComponent) tabbedPane.getComponentAt(index);
            c.requestFocus();
            c.grabFocus();
        });

        add(tabbedPane);
    }
}
