package cz.uhk.pgrf1.view;

import cz.uhk.pgrf1.controllers.Controller3D;
import cz.uhk.pgrf1.controllers.ui.UiController3D;
import cz.uhk.pgrf1.controllers.ui.UiController3DActionAdapter;
import cz.uhk.pgrf1.models3D.SolidsTableModel;
import cz.uhk.pgrf1.models3D.solids.Solid;
import cz.uhk.pgrf1.raster.Raster;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class Panel3D extends JPanel {
    private final UiController3D controller;

    private ImgPanel imgPanel;
    private SolidsDialog solidsDialog;

    public Panel3D() {
        super();
        this.controller = new UiController3D();
        initGui();
        initListeners();

        new Controller3D(this, controller);

        // lepší až na konci, aby to neukradla nějaká komponenta v případně složitějším UI
        this.setFocusable(true);
        //   imgPanel.grabFocus(); // důležité pro pozdější ovládání z klávesnice
    }

    private void initGui() {
        setLayout(new BorderLayout());

        SidePanel3D sidePanel = new SidePanel3D(controller);
        imgPanel = new ImgPanel();

        solidsDialog = new SolidsDialog(this);

        add(imgPanel, BorderLayout.CENTER);
        add(sidePanel, BorderLayout.LINE_END);

        JLabel axisLabel = new JLabel("Axis: X - Blue; Y - Green; Z - Red");
        add(axisLabel, BorderLayout.SOUTH);
    }

    private void initListeners() {
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char key = e.getKeyChar();
                if (key == 'r') {
                    controller.resetScene();
                } else if (key == 'm') {
                    controller.switchMode();
                }
            }
        });

        this.controller.addListener(new UiController3DActionAdapter() {
            @Override
            public void resetRequested() {
                solidsDialog.setVisible(false);
            }
        });
    }

    public Raster getRaster() {
        return imgPanel.getRaster();
    }

    public void resize() {
        imgPanel.resize();
    }

    public void clear() {
        imgPanel.clear();
    }

    public void showSolidsDialog(List<Solid> solids) {
        SolidsTableModel model = new SolidsTableModel(solids);
        model.setListeners(new ArrayList<>(controller.getListeners()));
        solidsDialog.setModel(model);
        solidsDialog.setVisible(true);
    }
}
