package cz.uhk.pgrf1.view;

import cz.uhk.pgrf1.controllers.Controller2D;
import cz.uhk.pgrf1.controllers.ui.UiController2D;
import cz.uhk.pgrf1.controllers.ui.UiController2DActionAdapter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * JPanel pro práci ve 2D
 */
public class Panel2D extends JPanel {
    private final UiController2D controller;

    private SidePanel2D sidePanel;
    private ImgPanel imgPanel;

    public Panel2D() {
        super();
        this.controller = new UiController2D();
        initGui();
        initListeners();

        new Controller2D(imgPanel, controller);

        imgPanel.setFocusable(true);
    }

    private void initGui() {
        setLayout(new BorderLayout());

        sidePanel = new SidePanel2D(controller);
        imgPanel = new ImgPanel();

        add(imgPanel, BorderLayout.CENTER);
        add(sidePanel, BorderLayout.LINE_END);
    }

    private void initListeners() {
        controller.addListener(new UiController2DActionAdapter() {
            @Override
            public void rasterizerChanged() {
                sidePanel.reloadRasterizerDescription();
            }

            @Override
            public void modeSwitched() {
                sidePanel.reloadModeDescription();
            }
        });

        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char key = e.getKeyChar();
                if (key == 'c') {
                    controller.hardClear();
                } else if (key == 'm') {
                    controller.switchMode();
                } else if (key == 'r') {
                    controller.switchRasterizer();
                }
            }
        });
    }
}
