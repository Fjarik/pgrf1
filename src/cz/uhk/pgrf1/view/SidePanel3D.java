package cz.uhk.pgrf1.view;

import cz.uhk.pgrf1.controllers.ui.UiController3D;
import cz.uhk.pgrf1.controllers.ui.UiController3DActionAdapter;
import cz.uhk.pgrf1.models3D.DrawMode3D;

import javax.swing.*;
import java.awt.*;

public class SidePanel3D extends JPanel {
    private final UiController3D controller;

    private JMultilineLabel controlsLabel;
    private JMultilineLabel descriptionLabel;
    private JLabel modeLabel;

    public SidePanel3D(UiController3D controller) {
        this.controller = controller;
        initPanel();
        this.setFocusable(false);

        controller.addListener(new UiController3DActionAdapter() {
            @Override
            public void perspectiveChanged() {
                reloadControlsDescription();
            }

            @Override
            public void modeSwitched() {
                reloadModeDescription();
            }
        });
    }

    private void initPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setPreferredSize(new Dimension(100, 1));

        JPanel buttonPanel = new JPanel(new GridLayout(10, 1));
        buttonPanel.setFocusable(false);

        JButton clearBtn = new JButton("[R]eset");
        clearBtn.addActionListener(e -> controller.resetScene());
        clearBtn.setFocusable(false);

        JButton modeBtn = new JButton("[M]ode");
        modeBtn.addActionListener(e -> controller.switchMode());
        modeBtn.setFocusable(false);

        JButton objectsBtn = new JButton("Solids");
        objectsBtn.addActionListener(e -> controller.showSolidsDialog());
        objectsBtn.setFocusable(false);

        buttonPanel.add(clearBtn);
        buttonPanel.add(modeBtn);
        buttonPanel.add(objectsBtn);

        JCheckBox useOrthogonalCheckBox = new JCheckBox("Orthogonal?", true);
        useOrthogonalCheckBox.addActionListener(x -> controller.setOrthogonal(useOrthogonalCheckBox.isSelected()));
        useOrthogonalCheckBox.setFocusable(false);

        JCheckBox animateCheckBox = new JCheckBox("Animate?", false);
        animateCheckBox.addActionListener(x -> controller.animate(animateCheckBox.isSelected()));
        animateCheckBox.setFocusable(false);

        SpinnerNumberModel precisionModel = new SpinnerNumberModel(controller.getCurvePrecision(), 10, 500, 10);
        precisionModel.addChangeListener(e -> controller.setCurvePrecision((int) precisionModel.getValue()));
        JSpinner precisionSpinner = new JSpinner(precisionModel);
        precisionSpinner.setFocusable(false);

        buttonPanel.add(useOrthogonalCheckBox);
        buttonPanel.add(animateCheckBox);
        buttonPanel.add(new JLabel("Precision:"));
        buttonPanel.add(precisionSpinner);

        this.add(buttonPanel);

        JPanel textPanel = new JPanel(new BorderLayout());

        modeLabel = new JLabel();
        textPanel.add(modeLabel, BorderLayout.NORTH);

        descriptionLabel = new JMultilineLabel();
        textPanel.add(descriptionLabel, BorderLayout.CENTER);

        controlsLabel = new JMultilineLabel();
        textPanel.add(controlsLabel, BorderLayout.SOUTH);

        this.add(textPanel);

        reloadModeDescription();
        reloadControlsDescription();
    }

    public void reloadModeDescription() {
        DrawMode3D mode = controller.getMode();

        modeLabel.setText(mode.getName());
        descriptionLabel.setText(mode.getDescription());
    }

    private void reloadControlsDescription() {
        if (controller.isOrthogonal()) {
            controlsLabel.setText(
                    "W - Up \n" +
                            "A - Left \n" +
                            "S - Down \n" +
                            "D - Right \n" +
                            "Space - Up \n" +
                            "Ctrl - Down \n"
            );
        } else {
            controlsLabel.setText(
                    "W - Forward \n" +
                            "A - Left \n" +
                            "S - Backwards \n" +
                            "D - Right \n" +
                            "Space - Up \n" +
                            "Ctrl - Down \n"
            );
        }
    }


}
