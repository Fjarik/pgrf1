package cz.uhk.pgrf1.view;

import cz.uhk.pgrf1.controllers.ui.UiController2D;
import cz.uhk.pgrf1.models.DrawMode;
import cz.uhk.pgrf1.models.RasterizerMode;

import javax.swing.*;
import java.awt.*;

public class SidePanel2D extends JPanel {

    private JMultilineLabel rasterizerLabel;
    private JMultilineLabel descriptionLabel;
    private JLabel modeLabel;
    private JSpinner dotSpaceSpinner;
    private final UiController2D controller;

    public SidePanel2D(UiController2D controller) {
        this.controller = controller;
        initPanel();
        this.setFocusable(false);
    }

    private void initPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.setPreferredSize(new Dimension(100, 1));

        JPanel buttonPanel = new JPanel(new GridLayout(10, 1));

        JButton clearBtn = new JButton("[C]lear");
        clearBtn.addActionListener(e -> controller.hardClear());

        JButton modeBtn = new JButton("[M]ode");
        modeBtn.addActionListener(e -> controller.switchMode());

        JButton rasterizersBtn = new JButton("[R]asterizer");
        rasterizersBtn.addActionListener(e -> controller.switchRasterizer());

        SpinnerNumberModel dotSpaceModel = new SpinnerNumberModel(controller.getDotsSpace(), 2, 30, 1);
        dotSpaceModel.addChangeListener(e -> controller.setDotsSpace((int) dotSpaceModel.getValue()));
        dotSpaceSpinner = new JSpinner(dotSpaceModel);
        dotSpaceSpinner.setVisible(false);

        buttonPanel.add(clearBtn);
        buttonPanel.add(modeBtn);
        buttonPanel.add(rasterizersBtn);
        buttonPanel.add(dotSpaceSpinner);

        JButton lineColorBtn = new JButton("Line color");
        lineColorBtn.addActionListener(x -> {
            Color newColor = JColorChooser.showDialog(
                    this.getParent(),
                    "Choose line color",
                    controller.getLineColor()
            );
            controller.setLineColor(newColor);
        });

        JButton fillColorBtn = new JButton("Fill color");
        fillColorBtn.setVisible(false);
        fillColorBtn.addActionListener(x -> {
            Color newColor = JColorChooser.showDialog(
                    this.getParent(),
                    "Choose fill color",
                    controller.getFillColor()
            );
            controller.setFillColor(newColor);
        });

        JCheckBox usePatternCheckBox = new JCheckBox("Use pattern?", false);
        usePatternCheckBox.setVisible(false);
        usePatternCheckBox.addActionListener(x -> {
            controller.setShouldUsePattern(usePatternCheckBox.isSelected());
        });

        JCheckBox fillCheckBox = new JCheckBox("Fill?", false);
        fillCheckBox.addActionListener(x -> {
            controller.setFillColor(fillCheckBox.isSelected() ? Color.PINK : null);
            fillColorBtn.setVisible(fillCheckBox.isSelected());
            usePatternCheckBox.setVisible(fillCheckBox.isSelected());
        });

        buttonPanel.add(lineColorBtn);
        buttonPanel.add(fillCheckBox);
        buttonPanel.add(fillColorBtn);
        buttonPanel.add(usePatternCheckBox);

        JCheckBox clipCheckBox = new JCheckBox("Clip polygon", false);
        clipCheckBox.addActionListener(x -> {
            controller.setClipEnabled(clipCheckBox.isSelected());
        });
        buttonPanel.add(clipCheckBox);
        JCheckBox clipReverseCheckBox = new JCheckBox("Reverse Clip", false);
        clipReverseCheckBox.addActionListener(x -> {
            controller.reverseClipPoly();
        });
        buttonPanel.add(clipReverseCheckBox);

        this.add(buttonPanel);

        JPanel textPanel = new JPanel(new BorderLayout());

        rasterizerLabel = new JMultilineLabel();
        textPanel.add(rasterizerLabel, BorderLayout.NORTH);

        descriptionLabel = new JMultilineLabel();
        textPanel.add(descriptionLabel, BorderLayout.CENTER);

        modeLabel = new JLabel();
        textPanel.add(modeLabel, BorderLayout.SOUTH);

        this.add(textPanel);

        reloadRasterizerDescription();
        reloadModeDescription();
    }

    public void reloadModeDescription() {
        DrawMode mode = controller.getMode();

        modeLabel.setText(mode.getName());
        descriptionLabel.setText(mode.getDescription());
    }

    public void reloadRasterizerDescription() {
        RasterizerMode mode = controller.getRasterizerMode();

        rasterizerLabel.setText(mode.getDescription());
        dotSpaceSpinner.setVisible(mode == RasterizerMode.DOTTED);
    }
}
