package cz.uhk.pgrf1.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class JMultilineLabel extends JTextArea {
    // Zdroj:
    // https://stackoverflow.com/questions/2152742/java-swing-multiline-labels
    // Jelikož se mi nepodařilo najít, že by ve Swingu byl víceřádkový label :(

    public JMultilineLabel() {
        this("");
    }

    public JMultilineLabel(String text) {
        super(text);
        setEditable(false);
        setCursor(null);
        setOpaque(false);
        setFocusable(false);
        setFont(UIManager.getFont("Label.font"));
        setWrapStyleWord(true);
        setLineWrap(true);
        setBorder(new EmptyBorder(5, 5, 5, 5));
        setAlignmentY(JLabel.CENTER_ALIGNMENT);
    }
}
