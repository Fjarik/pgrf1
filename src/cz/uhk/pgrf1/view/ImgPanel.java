package cz.uhk.pgrf1.view;

import cz.uhk.pgrf1.raster.Raster;
import cz.uhk.pgrf1.raster.RasterBufferedImage;

import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

public class ImgPanel extends JPanel {

    private Raster raster;
    private static final int FPS = 1000 / 20;
    public static final int WIDTH = 800, HEIGHT = 600;

    public ImgPanel() {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        raster = new RasterBufferedImage(WIDTH, HEIGHT, new Color(0x2f2f2f));
        // setLoop();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        raster.present(g);
        g.setColor(Color.WHITE);
        //  setLoop();
    }

    public Raster getRaster() {
        return raster;
    }

    public void resize() {
        if (this.getWidth() < 1 || this.getHeight() < 1)
            return;

        RasterBufferedImage newRaster = new RasterBufferedImage(this.getWidth(), this.getHeight(), raster.getBackgroundColour());
        newRaster.present(newRaster);

        raster = newRaster;
    }

    private void setLoop() {
        // časovač, který 30 krát za vteřinu obnoví obsah plátna aktuálním img
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                repaint();
            }
        }, 0, FPS);
    }

    public void clear() {
        raster.clear();
        this.repaint();
    }

}
