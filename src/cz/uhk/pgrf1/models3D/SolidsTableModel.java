package cz.uhk.pgrf1.models3D;

import cz.uhk.pgrf1.models3D.solids.Solid;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class SolidsTableModel extends AbstractTableModel {
    private final List<SolidInfo> info;
    private final List<SolidChangeListener> listeners;

    public SolidsTableModel(List<Solid> solids) {
        this.listeners = new ArrayList<>();
        this.info = solids.stream().map(SolidInfo::new).collect(Collectors.toList());
    }

    @Override
    public int getRowCount() {
        return info.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Name";
            case 1:
                return "Visible";
            case 2:
                return "Transform";
            case 3:
                return "ID";
            default:
                return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SolidInfo s = info.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return s.name;
            case 1:
                return s.isVisible;
            case 2:
                return s.transform;
            case 3:
                return s.id;
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 1)
            return true;

        if (columnIndex != 2)
            return false;

        SolidInfo s = info.get(rowIndex);
        return s.isVisible && s.allowTransform;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        SolidInfo s = info.get(rowIndex);

        if (!(aValue instanceof Boolean)) {
            return;
        }
        boolean val = (Boolean) aValue;
        if (columnIndex == 1) {
            s.isVisible = val;
            callListeners(x -> x.visibilityChanged(s.id, val));
            if (val || !s.transform) {
                fireTableDataChanged();
                return;
            }
        }
        s.transform = val;
        callListeners(x -> x.transformChanged(s.id, val));
        fireTableDataChanged();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1:
            case 2:
                return Boolean.class;
            default:
                return String.class;
        }
    }

    private void callListeners(Consumer<SolidChangeListener> action) {
        for (SolidChangeListener listener : listeners) {
            action.accept(listener);
        }
    }

    public void setListeners(List<SolidChangeListener> listeners) {
        this.listeners.clear();
        this.listeners.addAll(listeners);
    }

    private static class SolidInfo {
        private final UUID id;
        private final String name;
        private boolean isVisible;
        private boolean transform;
        private boolean allowTransform;

        public SolidInfo(Solid s) {
            this(s.getId(), s.getTypeName(), s.isVisible(), s.shouldTransform(), s.allowTransform());
        }

        public SolidInfo(UUID id, String name, boolean isVisible, boolean transform, boolean allowTransform) {
            this.id = id;
            this.name = name;
            this.isVisible = isVisible;
            this.transform = transform;
            this.allowTransform = allowTransform;
        }
    }
}

