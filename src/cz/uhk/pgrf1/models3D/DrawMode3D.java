package cz.uhk.pgrf1.models3D;

public enum DrawMode3D {
    MOVE("Move", "Move camera using mouse"),
    TRANSLATE("Translate", "Move objects using mouse. Left - by x & z, right by y & z axis"),
    ROTATE("Rotate", "Rotate objects using mouse"),
    SCALE("Scale", "Change scale of objects using mouse");

    private static final DrawMode3D[] allValues = values();

    private final String name;
    private final String description;

    DrawMode3D(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public DrawMode3D next() {
        return allValues[(this.ordinal() + 1) % allValues.length];
    }
}
