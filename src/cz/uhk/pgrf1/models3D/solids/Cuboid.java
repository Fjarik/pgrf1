package cz.uhk.pgrf1.models3D.solids;

import transforms.Point3D;

public class Cuboid extends Solid {

    @Override
    public String getTypeName() {
        return "Cuboid";
    }

    public Cuboid() {
        this.vertexBuffer.add(new Point3D(0, 0, 0)); // A 0
        this.vertexBuffer.add(new Point3D(10, 0, 0)); // B 1
        this.vertexBuffer.add(new Point3D(10, 10, 0)); // C 2
        this.vertexBuffer.add(new Point3D(0, 10, 0)); // D 3
        this.vertexBuffer.add(new Point3D(0, 0, 20)); // E 4
        this.vertexBuffer.add(new Point3D(10, 0, 20)); // F 5
        this.vertexBuffer.add(new Point3D(10, 10, 20)); // G 6
        this.vertexBuffer.add(new Point3D(0, 10, 20)); // H 7

        // A-B
        this.indexBuffer.add(0);
        this.indexBuffer.add(1);

        // B-C
        this.indexBuffer.add(1);
        this.indexBuffer.add(2);

        // C-D
        this.indexBuffer.add(2);
        this.indexBuffer.add(3);

        // D-A
        this.indexBuffer.add(3);
        this.indexBuffer.add(0);

        // E-F
        this.indexBuffer.add(4);
        this.indexBuffer.add(5);

        // F-G
        this.indexBuffer.add(5);
        this.indexBuffer.add(6);

        // G-H
        this.indexBuffer.add(6);
        this.indexBuffer.add(7);

        // H-E
        this.indexBuffer.add(7);
        this.indexBuffer.add(4);

        // A-E
        this.indexBuffer.add(0);
        this.indexBuffer.add(4);

        // B-F
        this.indexBuffer.add(1);
        this.indexBuffer.add(5);

        // C-G
        this.indexBuffer.add(2);
        this.indexBuffer.add(6);

        // D-H
        this.indexBuffer.add(3);
        this.indexBuffer.add(7);
    }
}
