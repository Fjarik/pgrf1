package cz.uhk.pgrf1.models3D.solids;

import cz.uhk.pgrf1.models3D.ColourBuffer;
import transforms.Point3D;

import java.awt.*;

/**
 * Falešný kužel :)
 */
public class Cone extends Solid {
    @Override
    public String getTypeName() {
        return "Cone";
    }

    public Cone() {
        this(5, 50);
    }

    public Cone(int connections, int precision) {
        Point3D center = new Point3D(0, 0, 0);
        double radius = 10;

        for (int i = 0; i < precision; i++) {
            vertexBuffer.add(Circle.computeCircle((double) i / precision, radius, center));
            if (i != 0) {
                indexBuffer.add(i - 1);
                indexBuffer.add(i);
            }
        }
        indexBuffer.add(0);
        indexBuffer.add(vertexBuffer.size() - 1);

        vertexBuffer.add(center.withZ(15));

        int topIndex = vertexBuffer.size() - 1;
        int step = (int) Math.round((double) topIndex / connections);

        for (int i = 0; i < vertexBuffer.size() - 1; i += step) {
            indexBuffer.add(topIndex);
            indexBuffer.add(i);
        }

        this.colourBuffer = ColourBuffer.oneColour(Color.WHITE);
    }
}
