package cz.uhk.pgrf1.models3D.solids;

import cz.uhk.pgrf1.models3D.ColourBuffer;
import transforms.Bicubic;
import transforms.Cubic;
import transforms.Point3D;

import java.awt.*;

public class Mesh extends Solid {
    @Override
    public String getTypeName() {
        return "Mesh";
    }

    protected static final Point3D[] points = new Point3D[]{
            new Point3D(-15, 0, 3),
            new Point3D(-15, -5, -2),
            new Point3D(-15, -10, -4),
            new Point3D(-15, -15, -6),

            new Point3D(-10, 0, 3),
            new Point3D(-10, -5, 3),
            new Point3D(-10, -10, -3),
            new Point3D(-10, -15, 0),

            new Point3D(-5, 0, 0),
            new Point3D(-5, -5, 0),
            new Point3D(-5, -10, 0),
            new Point3D(-5, -15, 0),

            new Point3D(0, 0, 0),
            new Point3D(0, -5, 0),
            new Point3D(0, -10, 2),
            new Point3D(0, -15, 6),
    };

    public Mesh() {
        Bicubic bicubic = new Bicubic(Cubic.BEZIER, points);

        int precision = 10;
        for (int i = 0; i <= precision; i++) {
            for (int j = 0; j <= precision; j++) {
                vertexBuffer.add(bicubic.compute((double) i / precision, (double) j / precision));
            }
        }

        // Sloupce a řádky

        int a = 0;
        for (int i = 0; i < vertexBuffer.size() - 1; i++) {
            if (a == precision) {
                a = 0;
                continue;
            }
            a++;
            this.indexBuffer.add(i);
            this.indexBuffer.add(i + 1);
        }

        for (int i = 0; i < vertexBuffer.size() - precision - 1; i++) {
            this.indexBuffer.add(i);
            this.indexBuffer.add(i + precision + 1);
        }

        this.colourBuffer = ColourBuffer.oneColour(new Color(196, 55, 252));
    }
}
