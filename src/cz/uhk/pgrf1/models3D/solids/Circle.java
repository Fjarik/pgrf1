package cz.uhk.pgrf1.models3D.solids;

import cz.uhk.pgrf1.models3D.ColourBuffer;
import transforms.Point3D;

import java.awt.*;

public class Circle extends Solid {
    @Override
    public String getTypeName() {
        return "Circle";
    }

    public Circle() {
        this(50);
    }

    public Circle(int precision) {
        Point3D center = new Point3D(0, 0, 0);
        double radius = 5;

        for (int i = 0; i < precision; i++) {
            vertexBuffer.add(computeCircle((double) i / precision, radius, center));
            if (i != 0) {
                indexBuffer.add(i - 1);
                indexBuffer.add(i);
            }
        }
        indexBuffer.add(0);
        indexBuffer.add(vertexBuffer.size() - 1);

        this.isVisible = false;
        this.shouldTransform = false;
        this.colourBuffer = ColourBuffer.oneColour(Color.WHITE);
    }

    public static Point3D computeCircle(double param, double radius, Point3D center) {
        param = param * 2 * Math.PI;
        param = Math.max(0, param);
        param = Math.min(2 * Math.PI, param);

        return center.add(new Point3D(radius * Math.cos(param), radius * Math.sin(param), 0));
    }
}
