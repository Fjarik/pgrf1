package cz.uhk.pgrf1.models3D.solids;

import cz.uhk.pgrf1.models3D.ColourBuffer;
import transforms.Point3D;

import java.awt.*;

public class Edge extends Solid {

    @Override
    public String getTypeName() {
        return "Edge";
    }

    public Edge() {
        this.vertexBuffer.add(new Point3D(0, 0, 0)); // A 0
        this.vertexBuffer.add(new Point3D(1, 1, 1)); // B 1

        this.colourBuffer = ColourBuffer.oneColour(Color.PINK);

        // A-B
        this.indexBuffer.add(0);
        this.indexBuffer.add(1);
    }
}
