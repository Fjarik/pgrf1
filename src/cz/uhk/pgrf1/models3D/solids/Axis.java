package cz.uhk.pgrf1.models3D.solids;

import transforms.Point3D;

import java.awt.*;

public class Axis extends Solid {

    @Override
    public String getTypeName() {
        return "Axis";
    }

    @Override
    public boolean allowTransform() {
        return false;
    }

    public Axis() {
        int size = 25;

        this.vertexBuffer.add(new Point3D(0, 0, 0)); // S 0
        this.vertexBuffer.add(new Point3D(size, 0, 0)); // X 1
        this.vertexBuffer.add(new Point3D(0, size, 0)); // Y 2
        this.vertexBuffer.add(new Point3D(0, 0, size)); // Y 3

        this.indexBuffer.add(0);
        this.indexBuffer.add(1);
        this.colourBuffer.add(Color.BLUE); // X

        this.indexBuffer.add(0);
        this.indexBuffer.add(2);
        this.colourBuffer.add(Color.GREEN); // Y

        this.indexBuffer.add(0);
        this.indexBuffer.add(3);
        this.colourBuffer.add(Color.RED); // Z

        shouldTransform = false;
    }
}
