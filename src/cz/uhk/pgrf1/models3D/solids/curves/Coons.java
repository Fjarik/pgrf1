package cz.uhk.pgrf1.models3D.solids.curves;

import transforms.Cubic;

public class Coons extends Curve {
    @Override
    public String getTypeName() {
        return "Coons " + super.getTypeName();
    }

    public Coons(int precision) {
        super(Cubic.COONS, precision);
        this.isVisible = false;
        this.shouldTransform = false;
    }
}
