package cz.uhk.pgrf1.models3D.solids.curves;

import transforms.Cubic;

public class Ferguson extends Curve {
    @Override
    public String getTypeName() {
        return "Ferguson " + super.getTypeName();
    }

    public Ferguson(int precision) {
        super(Cubic.FERGUSON, precision);
        this.isVisible = false;
        this.shouldTransform = false;
    }
}
