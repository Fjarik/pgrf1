package cz.uhk.pgrf1.models3D.solids.curves;

import cz.uhk.pgrf1.models3D.ColourBuffer;
import cz.uhk.pgrf1.models3D.solids.Solid;
import transforms.Cubic;
import transforms.Mat4;
import transforms.Point3D;

import java.awt.*;

public abstract class Curve extends Solid {

    @Override
    public String getTypeName() {
        return "Curve";
    }

    protected final Cubic cubic;

    protected static final Point3D[] points = new Point3D[]{
            new Point3D(-10, -10, 10),
            new Point3D(-10, -20, 0),
            new Point3D(0, -10, 10),
            new Point3D(-10, -10, 20),
    };

    public Curve(Mat4 cubic, int precision) {
        this(cubic, precision, points);
    }

    public Curve(Mat4 cubic, int precision, Point3D[] points) {
        this.cubic = new Cubic(cubic, points);
        setPrecision(precision);
        this.colourBuffer = ColourBuffer.oneColour(Color.CYAN);
    }

    public void setPrecision(int precision) {
        vertexBuffer.clear();
        indexBuffer.clear();
        for (int i = 0; i < precision; i++) {
            vertexBuffer.add(this.cubic.compute((double) i / precision));
            if (i != 0) {
                indexBuffer.add(i - 1);
                indexBuffer.add(i);
            }
        }
    }

}
