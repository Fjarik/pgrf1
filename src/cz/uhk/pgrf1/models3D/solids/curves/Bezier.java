package cz.uhk.pgrf1.models3D.solids.curves;

import transforms.Cubic;

public class Bezier extends Curve {

    @Override
    public String getTypeName() {
        return "Bezier " + super.getTypeName();
    }

    public Bezier(int precision) {
        super(Cubic.BEZIER, precision);
    }
}
