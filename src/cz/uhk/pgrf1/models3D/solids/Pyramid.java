package cz.uhk.pgrf1.models3D.solids;

import cz.uhk.pgrf1.models3D.ColourBuffer;
import transforms.Point3D;

import java.awt.*;

public class Pyramid extends Solid {

    @Override
    public String getTypeName() {
        return "Pyramid";
    }

    public Pyramid() {
        this.vertexBuffer.add(new Point3D(0, 10, 0)); // A 0
        this.vertexBuffer.add(new Point3D(-10, -10, -10)); // B 1
        this.vertexBuffer.add(new Point3D(10, -10, -10)); // C 2
        this.vertexBuffer.add(new Point3D(10, -10, 10)); // D 3
        this.vertexBuffer.add(new Point3D(-10, -10, 10)); // E 4

        // A-B
        this.indexBuffer.add(0);
        this.indexBuffer.add(1);

        // A-C
        this.indexBuffer.add(0);
        this.indexBuffer.add(2);

        // A-D
        this.indexBuffer.add(0);
        this.indexBuffer.add(3);

        // A-E
        this.indexBuffer.add(0);
        this.indexBuffer.add(4);

        // B-C
        this.indexBuffer.add(1);
        this.indexBuffer.add(2);

        // C-D
        this.indexBuffer.add(2);
        this.indexBuffer.add(3);

        // D-E
        this.indexBuffer.add(3);
        this.indexBuffer.add(4);

        // E-B
        this.indexBuffer.add(4);
        this.indexBuffer.add(1);

        this.colourBuffer = ColourBuffer.oneColour(Color.PINK);
    }
}
