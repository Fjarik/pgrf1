package cz.uhk.pgrf1.models3D.solids;

import cz.uhk.pgrf1.models3D.ColourBuffer;
import transforms.Mat4;
import transforms.Mat4Identity;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class Solid {

    protected final UUID id;

    protected List<Point3D> vertexBuffer;

    protected List<Integer> indexBuffer;

    protected ColourBuffer colourBuffer;

    protected Mat4 modelTransformation = new Mat4Identity();

    protected boolean isVisible;

    protected boolean shouldTransform;

    protected Solid() {
        this.id = UUID.randomUUID();
        this.isVisible = true;
        this.shouldTransform = true;
        this.vertexBuffer = new ArrayList<>();
        this.indexBuffer = new ArrayList<>();
        this.colourBuffer = ColourBuffer.empty();
    }

    public UUID getId() {
        return id;
    }

    public List<Point3D> getVertexBuffer() {
        return vertexBuffer;
    }

    public List<Integer> getIndexBuffer() {
        return indexBuffer;
    }

    public ColourBuffer getColourBuffer() {
        return colourBuffer;
    }

    public Mat4 getModelTransformation() {
        return modelTransformation;
    }

    public void transform(Mat4 trans) {
        if (trans == null)
            return;
        modelTransformation = modelTransformation.mul(trans);
    }

    public boolean isVisible() {
        return isVisible;
    }

    public boolean shouldTransform() {
        return shouldTransform;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public void setShouldTransform(boolean shouldTransform) {
        if (!allowTransform())
            return;

        this.shouldTransform = shouldTransform;
    }

    public abstract String getTypeName();

    public boolean allowTransform() {
        return true;
    }
}
