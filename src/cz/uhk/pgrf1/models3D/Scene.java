package cz.uhk.pgrf1.models3D;

import cz.uhk.pgrf1.models3D.solids.Solid;
import transforms.Mat4;
import transforms.Mat4Identity;

import java.util.ArrayList;
import java.util.List;

public class Scene {

    private final List<Solid> solids;

    protected Mat4 viewTransformation = new Mat4Identity();
    protected Mat4 projectionTransformation = new Mat4Identity();

    public Scene() {
        this.solids = new ArrayList<>();
    }

    public void add(Solid solid) {
        solids.add(solid);
    }

    public List<Solid> getSolids() {
        return solids;
    }

    public void setViewTrans(Mat4 viewTransformation) {
        this.viewTransformation = viewTransformation;
    }

    public void setProjectionTrans(Mat4 projectionTransformation) {
        this.projectionTransformation = projectionTransformation;
    }

    public Mat4 getViewTrans() {
        return viewTransformation;
    }

    public Mat4 getProjectionTrans() {
        return projectionTransformation;
    }

    public void transformSolids(Mat4 trans) {
        solids.stream()
                .filter(x -> x.isVisible() && x.shouldTransform())
                .forEach(x -> x.transform(trans));
    }
}
