package cz.uhk.pgrf1.models3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class ColourBuffer {

    private final List<Color> buffer;

    private ColourBuffer() {
        buffer = new ArrayList<>();
    }

    public void add(Color c) {
        buffer.add(c);
    }

    public Color get(int index, Color defaultColor) {
        return get(index).orElse(defaultColor);
    }

    public Optional<Color> get(int index) {
        if (index < 0)
            return Optional.empty(); // Záporný index

        int size = buffer.size();
        if (size == 0)
            return Optional.empty(); // Žádné barvičky

        if (size == 1)
            return Optional.of(buffer.get(0)); // Vrátit první barvu

        if (index > size - 1)
            return Optional.empty(); // Neplatný index (větší než list)

        return Optional.of(buffer.get(index));
    }

    public static ColourBuffer random() {
        Random rnd = new Random();
        return oneColour(new Color(rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255)));
    }

    public static ColourBuffer oneColour(Color c) {
        ColourBuffer b = new ColourBuffer();
        b.add(c);
        return b;
    }

    public static ColourBuffer empty() {
        return new ColourBuffer();
    }
}
