package cz.uhk.pgrf1.models3D;

import java.util.EventListener;
import java.util.UUID;

public interface SolidChangeListener extends EventListener {
    void visibilityChanged(UUID id, boolean newValue);

    void transformChanged(UUID id, boolean newValue);
}
