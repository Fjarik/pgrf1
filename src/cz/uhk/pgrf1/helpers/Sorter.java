package cz.uhk.pgrf1.helpers;

import java.util.List;

public class Sorter {

    /**
     * Bubble sort - Jeden z nejznámějších řadících algoritmů
     * Vždy se porovnávají 2 sousední prvky a prohodí se pokud je jeden vyšší či nižší
     * Poměrně pomalý
     */
    public static void bubbleSort(List<Integer> numbers) {
        for (int i = 0; i < numbers.size() - 1; i++) {
            for (int j = 0; j < numbers.size() - i - 1; j++) {
                if (numbers.get(j) > numbers.get(j + 1)) {
                    int tmp = numbers.get(j);
                    numbers.set(j, numbers.get(j + 1));
                    numbers.set(j + 1, tmp);
                }
            }
        }
    }

    /**
     * Insertion sort - Řazení vkládáním
     * Jeden z nejrychlejších z jednoduchých algoritmů
     * "Rozděluje" pole na 2 části - setříděné a nesetříděné, postupně "vkládá" nesetříděné do setříděných
     */
    public static void insertionSort(List<Integer> numbers) {
        for (int i = 0; i < numbers.size() - 1; i++) {
            int j = i + 1;
            int tmp = numbers.get(j);
            while (j > 0 && tmp < numbers.get(j - 1)) {
                numbers.set(j, numbers.get(j - 1));
                j--;
            }
            numbers.set(j, tmp);
        }
    }

    /**
     * Selection sort
     * Rychlejší než bubble sort, pomalejší než insertion sort
     * Řazení podle nejmenšího či největšího čísla
     */
    public static void selectionSort(List<Integer> numbers) {
        for (int i = 0; i < numbers.size() - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < numbers.size(); j++) {
                if (numbers.get(j) < numbers.get(minIndex)) {
                    minIndex = j;
                }
            }
            int tmp = numbers.get(i);
            numbers.set(i, numbers.get(minIndex));
            numbers.set(minIndex, tmp);
        }
    }
}
