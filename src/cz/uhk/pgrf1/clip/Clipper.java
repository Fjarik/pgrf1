package cz.uhk.pgrf1.clip;

import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.models.graphics.ClipPolyLine;
import cz.uhk.pgrf1.models.graphics.GraphicsPointedObject;
import cz.uhk.pgrf1.models.graphics.PolyLine;

import java.util.ArrayList;
import java.util.List;

public class Clipper {

    public static GraphicsPointedObject clip(ClipPolyLine clip, GraphicsPointedObject source) {
        return clip(clip.getPoints(), source);
    }

    public static GraphicsPointedObject clip(List<Point> clipPoints, GraphicsPointedObject source) {
        if (clipPoints.size() < 3)
            return null;
        List<Point> points = new ArrayList<>(source.getPoints());

        if (points.size() < 3)
            return null;

        List<Point> res = points;
        Point one = clipPoints.get(clipPoints.size() - 1);
        for (Point two : clipPoints) {
            res = clipEdge(points, new Edge(one, two));
            points = res;
            one = two;
        }

        return new PolyLine(res, source.getColour(), source.getFillColour(), false);
    }

    private static List<Point> clipEdge(List<Point> points, Edge edge) {
        if (points.size() < 2)
            return points;

        List<Point> res = new ArrayList<>();
        Point v1 = points.get(points.size() - 1);
        for (Point v2 : points) {
            if (edge.isInside(v2)) {
                if (!edge.isInside(v1))
                    res.add(edge.intersection(v1, v2));
                res.add(v2);
            } else if (edge.isInside(v1)) {
                res.add(edge.intersection(v1, v2));
            }
            v1 = v2;
        }
        return res;
    }

    private static class Edge {
        private final Point one;
        private final Point two;

        public Edge(Point one, Point two) {
            this.one = one;
            this.two = two;
        }

        public boolean isInside(Point p) {
            // Normála
            Point normal = two.subtract(one).normal();
            Point v = p.subtract(one);
            int distance = normal.getX() * v.getX() + normal.getY() * v.getY();
            return distance < 0;
        }

        public Point intersection(Point p1, Point p2) {
            // https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
            /*
                Podle vzorečku:
                (x a y)1 = p1
                2 = p2
                3 = one
                4 = two
             */

            int a = p1.getX() * p2.getY() - p1.getY() * p2.getX();
            int b = one.getX() * two.getY() - one.getY() * two.getX();
            int dy = one.getY() - two.getY();
            int dx = one.getX() - two.getX();
            int pDx = p1.getX() - p2.getX();
            int pDy = p1.getY() - p2.getY();

            float d = ((pDx * dy) - (dx * pDy));

            float px = ((a * dx) - (b * pDx)) / d;
            float py = ((a * dy) - (b * pDy)) / d;
            return new Point(px, py);
        }
    }
}
