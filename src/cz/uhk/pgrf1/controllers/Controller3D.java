package cz.uhk.pgrf1.controllers;

import cz.uhk.pgrf1.controllers.ui.UiController3D;
import cz.uhk.pgrf1.controllers.ui.UiController3DActionListener;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.models3D.Scene;
import cz.uhk.pgrf1.models3D.solids.*;
import cz.uhk.pgrf1.models3D.solids.curves.Bezier;
import cz.uhk.pgrf1.models3D.solids.curves.Coons;
import cz.uhk.pgrf1.models3D.solids.curves.Curve;
import cz.uhk.pgrf1.models3D.solids.curves.Ferguson;
import cz.uhk.pgrf1.raster.Raster;
import cz.uhk.pgrf1.raster.rasterizers.FilledLineRasterizer;
import cz.uhk.pgrf1.raster.rasterizers.LineRasterizer;
import cz.uhk.pgrf1.render.Renderer;
import cz.uhk.pgrf1.render.WireframeEngine;
import cz.uhk.pgrf1.view.Panel3D;
import transforms.*;

import javax.swing.*;
import java.awt.event.*;
import java.util.UUID;

public class Controller3D implements Controller {

    private final Panel3D panel;
    private final UiController3D controller;
    private final Timer animationTimer;

    private Renderer engine;
    private Scene scene;
    private Point startPoint;

    private Camera camera;

    public Controller3D(Panel3D panel, UiController3D controller) {
        this.panel = panel;
        this.controller = controller;
        this.animationTimer = new Timer(10, x -> animate());
        animationTimer.setRepeats(true);

        initObjects(panel.getRaster());
        initListeners();

        panel.requestFocus();
        panel.requestFocusInWindow();
        redraw();
    }

    @Override
    public void initObjects(Raster raster) {
        LineRasterizer rasterizer = new FilledLineRasterizer(raster);

        scene = new Scene();
        scene.add(new Axis());

        Solid cube = new Cube();
        cube.transform(new Mat4Transl(5, 5, 0));
        scene.add(cube);

        Solid cuboid = new Cuboid();
        cuboid.transform(new Mat4Transl(0, -15, 0));
        scene.add(cuboid);

        Solid pyramid = new Pyramid();
        // Je pro mě jednodušší to transformovat než ručně měnit mody
        pyramid.transform(new Mat4Transl(10, 10, -10).mul(new Mat4RotX(Math.PI / 2)));
        scene.add(pyramid);

        int precision = controller.getCurvePrecision();
        Solid bezier = new Bezier(precision);
        scene.add(bezier);
        Solid coons = new Coons(precision);
        scene.add(coons);
        Solid ferguson = new Ferguson(precision);
        scene.add(ferguson);

        Solid circle = new Circle();
        scene.add(circle);

        Solid cone = new Cone();
        cone.transform(new Mat4Transl(-10, 10, 0));
        scene.add(cone);

        Solid mesh = new Mesh();
        scene.add(mesh);

        engine = new WireframeEngine(raster, rasterizer);

        camera = getStartCamera();

        changePerspective(controller.isOrthogonal());
    }

    @Override
    public void initListeners() {
        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                initObjects(panel.getRaster());
                redraw();
            }
        });

        controller.addListener(new UiController3DActionListener() {
            @Override
            public void perspectiveChanged() {
                changePerspective(controller.isOrthogonal());
            }

            @Override
            public void resetRequested() {
                initObjects(panel.getRaster());
                redraw();
            }

            @Override
            public void modeSwitched() {
                redraw();
            }

            @Override
            public void solidsDialogRequested() {
                panel.showSolidsDialog(scene.getSolids());
            }

            @Override
            public void visibilityChanged(UUID id, boolean newValue) {
                Solid s = getSolid(id);
                if (s != null)
                    s.setVisible(newValue);

                redraw();
            }

            @Override
            public void transformChanged(UUID id, boolean newValue) {
                Solid s = getSolid(id);
                if (s != null)
                    s.setShouldTransform(newValue);

                redraw();
            }

            @Override
            public void animationChangeRequested(boolean enabled) {
                if (!enabled && animationTimer.isRunning()) {
                    animationTimer.stop();
                    return;
                }
                animationTimer.start();
            }

            @Override
            public void curvePrecisionChanged() {
                int newPrecision = controller.getCurvePrecision();
                scene.getSolids()
                        .stream()
                        .filter(x -> x instanceof Curve)
                        .map(x -> ((Curve) x))
                        .forEach(x -> x.setPrecision(newPrecision));
                redraw();
            }

            private Solid getSolid(UUID id) {
                for (Solid solid : scene.getSolids()) {
                    if (!solid.getId().equals(id)) {
                        continue;
                    }
                    return solid;
                }
                return null;
            }

        });

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                startPoint = new Point(e);
            }
        });

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                float dx = e.getX() - startPoint.getX();
                float dy = e.getY() - startPoint.getY();

                startPoint = new Point(e);
                Raster raster = panel.getRaster();

                switch (controller.getMode()) {
                    case MOVE:

                        double azimuthChange = dx * Math.PI / raster.getWidth();
                        double zenithChange = dy * Math.PI / raster.getHeight();

                        camera = camera.addAzimuth(azimuthChange).addZenith(zenithChange);
                        break;
                    case TRANSLATE:
                        double dz = -dy / 6;
                        double diff = dx / 4;
                        Mat4 trans = new Mat4Identity();
                        if (SwingUtilities.isLeftMouseButton(e)) {
                            trans = new Mat4Transl(diff, 0, dz);
                        } else if (SwingUtilities.isRightMouseButton(e)) {
                            trans = new Mat4Transl(0, -diff, dz);
                        }
                        scene.transformSolids(trans);
                        break;
                    case ROTATE:
                        scene.transformSolids(new Mat4RotXYZ(0, -dy * Math.PI / 180, dx * Math.PI / 180));
                        // scene.transformSolids(new Mat4RotX(-dy * Math.PI / 180));
                        // scene.transformSolids(new Mat4RotY(-dy * Math.PI / 180));
                        // scene.transformSolids(new Mat4RotZ(-dy * Math.PI / 180));
                        break;
                    case SCALE:
                        double dyProportional = dy / raster.getHeight();
                        double dxProportional = dx / raster.getWidth();

                        scene.transformSolids(new Mat4Scale(1 + dxProportional * 4, 1 + dyProportional * 4, 1));
                        break;
                }
                redraw();
            }
        });

        panel.addMouseWheelListener(e -> {
            if (controller.isOrthogonal())
                return;

            double step = 0.5;
            if (e.getWheelRotation() < 0) {
                camera = camera.forward(step);
            } else {
                camera = camera.backward(step);
            }
            redraw();
        });

        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                double step = 0.5;

                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                    case KeyEvent.VK_W:
                        camera = controller.isOrthogonal() ? camera.up(step) : camera.forward(step);
                        break;
                    case KeyEvent.VK_DOWN:
                    case KeyEvent.VK_S:
                        camera = controller.isOrthogonal() ? camera.down(step) : camera.backward(step);
                        break;
                    case KeyEvent.VK_LEFT:
                    case KeyEvent.VK_A:
                        camera = camera.left(step);
                        break;
                    case KeyEvent.VK_RIGHT:
                    case KeyEvent.VK_D:
                        camera = camera.right(step);
                        break;
                    case KeyEvent.VK_SPACE:
                        camera = camera.up(step);
                        break;
                    case KeyEvent.VK_CONTROL:
                        camera = camera.down(step);
                        break;
                }

                redraw();
            }
        });

    }

    private void changePerspective(boolean isOrthogonal) {
        double zn = 0.1d;
        double zf = 300d;
        double part = 8;
        Raster r = panel.getRaster();
        if (isOrthogonal) {
            scene.setProjectionTrans(new Mat4OrthoRH(r.getWidth() / part, r.getHeight() / part, zn, zf));
        } else {
            scene.setProjectionTrans(new Mat4PerspRH(Math.PI / 2, r.getHeight() / (double) r.getWidth(), zn, zf));
        }
        redraw();
    }

    private void animate() {
        scene.transformSolids(new Mat4RotZ(Math.PI / 180));
        redraw();
    }

    private Camera getStartCamera() {
        return new Camera()
                .withPosition(new Vec3D(-20, -20, -10))
                .withAzimuth(0.71)
                .withZenith(0.45)
                .withFirstPerson(true);
    }

    private void redraw() {
        panel.clear();

        scene.setViewTrans(camera.getViewMatrix());
        engine.render(scene);

        panel.repaint();
    }

}
