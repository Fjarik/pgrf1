package cz.uhk.pgrf1.controllers.ui;

import java.util.UUID;

public abstract class UiController3DActionAdapter implements UiController3DActionListener {

    @Override
    public void perspectiveChanged() {

    }

    @Override
    public void resetRequested() {
    }

    @Override
    public void modeSwitched() {

    }

    @Override
    public void solidsDialogRequested() {

    }

    @Override
    public void visibilityChanged(UUID id, boolean newValue) {

    }

    @Override
    public void transformChanged(UUID id, boolean newValue) {

    }

    @Override
    public void animationChangeRequested(boolean enabled) {

    }

    @Override
    public void curvePrecisionChanged() {

    }
}
