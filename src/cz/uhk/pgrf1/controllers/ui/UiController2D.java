package cz.uhk.pgrf1.controllers.ui;

import cz.uhk.pgrf1.models.DrawMode;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.models.RasterizerMode;
import cz.uhk.pgrf1.models.graphics.ClipPolyLine;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class UiController2D {
    private DrawMode mode;
    private RasterizerMode rasterizerMode;
    private int dotsSpace = 7;
    private Color lineColor;
    private Color fillColor;
    private final ClipPolyLine clipPoly;
    private boolean clipEnabled;
    private boolean shouldUsePattern;
    private final List<UiController2DActionListener> listeners;

    public UiController2D() {
        this.mode = DrawMode.LINE;
        this.rasterizerMode = RasterizerMode.NORMAL;
        this.lineColor = Color.YELLOW;
        this.fillColor = null;
        this.clipPoly = getInitClipPoly();
        this.clipEnabled = false;
        this.shouldUsePattern = false;
        this.listeners = new ArrayList<>();
    }

    private ClipPolyLine getInitClipPoly() {
        ClipPolyLine poly = new ClipPolyLine(new Point(100, 50), new Color(0x89CFF0));
        poly.addPoint(new Point(50, 200));
        poly.addPoint(new Point(350, 350));
        poly.addPoint(new Point(450, 250));
        poly.addPoint(new Point(350, 150));
        return poly;
    }

    public Optional<ClipPolyLine> getClipPoly() {
        return clipEnabled ? Optional.of(clipPoly) : Optional.empty();
    }

    public void reverseClipPoly() {
        this.clipPoly.reverse();
    }

    public void setClipEnabled(boolean clipEnabled) {
        this.clipEnabled = clipEnabled;
        raiseListeners(UiController2DActionListener::clipPolyChanged);
    }

    public DrawMode getMode() {
        return mode;
    }

    public RasterizerMode getRasterizerMode() {
        return rasterizerMode;
    }

    public void switchMode() {
        mode = mode.next();
        raiseListeners(UiController2DActionListener::modeSwitched);
    }

    public void hardClear() {
        raiseListeners(UiController2DActionListener::clearRequested);
    }

    public void switchRasterizer() {
        rasterizerMode = rasterizerMode.next();
        raiseRasterizerChanged();
    }

    public int getDotsSpace() {
        return dotsSpace;
    }

    public void setDotsSpace(int dotsSpace) {
        this.dotsSpace = dotsSpace;
        raiseRasterizerChanged();
    }

    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public boolean getShouldUsePattern() {
        return shouldUsePattern;
    }

    public void setShouldUsePattern(boolean shouldUsePattern) {
        this.shouldUsePattern = shouldUsePattern;
    }

    public void addListener(UiController2DActionListener listener) {
        this.listeners.add(listener);
    }

    private void raiseRasterizerChanged() {
        raiseListeners(UiController2DActionListener::rasterizerChanged);
    }

    private void raiseListeners(Consumer<UiController2DActionListener> action) {
        for (UiController2DActionListener listener : listeners) {
            action.accept(listener);
        }
    }
}
