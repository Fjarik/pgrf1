package cz.uhk.pgrf1.controllers.ui;

import java.util.EventListener;

public interface UiController2DActionListener extends EventListener {

    void rasterizerChanged();

    void modeSwitched();

    void clipPolyChanged();

    void clearRequested();
}
