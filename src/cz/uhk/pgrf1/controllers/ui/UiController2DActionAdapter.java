package cz.uhk.pgrf1.controllers.ui;

public abstract class UiController2DActionAdapter implements UiController2DActionListener {

    @Override
    public void rasterizerChanged() {

    }

    @Override
    public void modeSwitched() {

    }

    @Override
    public void clipPolyChanged() {

    }

    @Override
    public void clearRequested() {

    }
}
