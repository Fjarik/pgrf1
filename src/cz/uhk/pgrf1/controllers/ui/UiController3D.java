package cz.uhk.pgrf1.controllers.ui;

import cz.uhk.pgrf1.models3D.DrawMode3D;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class UiController3D {

    private final List<UiController3DActionListener> listeners;

    private DrawMode3D mode;
    private boolean isOrthogonal;
    private int curvePrecision;

    public UiController3D() {
        this.mode = DrawMode3D.MOVE;
        this.isOrthogonal = true;
        this.curvePrecision = 50;
        this.listeners = new ArrayList<>();
    }

    public DrawMode3D getMode() {
        return mode;
    }

    public void switchMode() {
        setMode(mode.next());
    }

    private void setMode(DrawMode3D newMode) {
        this.mode = newMode;
        raiseListeners(UiController3DActionListener::modeSwitched);
    }

    public void setOrthogonal(boolean isOrthogonal) {
        this.isOrthogonal = isOrthogonal;
        raiseListeners(UiController3DActionListener::perspectiveChanged);
    }

    public boolean isOrthogonal() {
        return isOrthogonal;
    }

    public void resetScene() {
        setMode(DrawMode3D.MOVE);
        raiseListeners(UiController3DActionListener::resetRequested);
    }

    public void showSolidsDialog() {
        raiseListeners(UiController3DActionListener::solidsDialogRequested);
    }

    public void animate(boolean enabled) {
        raiseListeners(x -> x.animationChangeRequested(enabled));
    }

    public int getCurvePrecision() {
        return curvePrecision;
    }

    public void setCurvePrecision(int curvePrecision) {
        this.curvePrecision = curvePrecision;
        raiseListeners(UiController3DActionListener::curvePrecisionChanged);
    }

    public void addListener(UiController3DActionListener listener) {
        this.listeners.add(listener);
    }

    public List<UiController3DActionListener> getListeners() {
        return listeners;
    }

    private void raiseListeners(Consumer<UiController3DActionListener> action) {
        for (UiController3DActionListener listener : listeners) {
            action.accept(listener);
        }
    }
}
