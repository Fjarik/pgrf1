package cz.uhk.pgrf1.controllers.ui;

import cz.uhk.pgrf1.models3D.SolidChangeListener;

import java.util.EventListener;

public interface UiController3DActionListener extends EventListener, SolidChangeListener {

    void perspectiveChanged();

    void resetRequested();

    void modeSwitched();

    void solidsDialogRequested();

    void animationChangeRequested(boolean enabled);

    void curvePrecisionChanged();
}
