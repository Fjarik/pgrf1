package cz.uhk.pgrf1.controllers;

import cz.uhk.pgrf1.clip.Clipper;
import cz.uhk.pgrf1.controllers.ui.UiController2D;
import cz.uhk.pgrf1.controllers.ui.UiController2DActionListener;
import cz.uhk.pgrf1.models.DrawMode;
import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;
import cz.uhk.pgrf1.models.graphics.Polygon;
import cz.uhk.pgrf1.models.graphics.*;
import cz.uhk.pgrf1.raster.Raster;
import cz.uhk.pgrf1.raster.fill.Pattern;
import cz.uhk.pgrf1.raster.fill.scanline.ScanLineFiller;
import cz.uhk.pgrf1.raster.fill.seedfill.SeedFillerBase;
import cz.uhk.pgrf1.raster.fill.seedfill.SeedFillerQueue;
import cz.uhk.pgrf1.raster.rasterizers.DottedLineRasterizer;
import cz.uhk.pgrf1.raster.rasterizers.FilledLineRasterizer;
import cz.uhk.pgrf1.raster.rasterizers.LineRasterizer;
import cz.uhk.pgrf1.view.ImgPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.*;

public class Controller2D implements Controller {

    private static final Color debugColor = Color.RED;

    private final ImgPanel panel;
    private final UiController2D controller;
    private final Pattern pattern;

    private LineRasterizer rasterizer;
    private SeedFillerBase seedFiller;
    private ScanLineFiller scanLineFiller;

    private Point start;
    private final List<GraphicsObject> objects;
    private Line debugLine;
    private PolyLine debugPoly;
    private Polygon debugPolygon;

    public Controller2D(ImgPanel panel, UiController2D controller) {
        this.panel = panel;
        this.controller = controller;

        objects = new ArrayList<>();

        pattern = getBasicPattern();
        initObjects(panel.getRaster());
        initListeners();

        panel.requestFocus();
        panel.requestFocusInWindow();
        redraw();
    }

    @Override
    public void initObjects(Raster raster) {
        scanLineFiller = new ScanLineFiller(raster);
        initRasterizer(raster);
    }

    private void initRasterizer(Raster raster) {
        switch (controller.getRasterizerMode()) {
            case NORMAL:
                rasterizer = new FilledLineRasterizer(raster);
                break;
            case DOTTED:
                rasterizer = new DottedLineRasterizer(raster, controller.getDotsSpace());
                break;
        }
    }

    private Pattern getBasicPattern() {
        /*
            .........
            ....*....
            ...***...
            .*******.
            .*******.
            .*******.
            .**...**.
            .**...**.
            .**...**.
            .........
         */
        return new Pattern(new Color[][]{
                new Color[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN},
                new Color[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.BLUE, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN},
                new Color[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.BLUE, Color.BLUE, Color.BLUE, Color.GREEN, Color.GREEN, Color.GREEN},
                new Color[]{Color.GREEN, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.GREEN},
                new Color[]{Color.GREEN, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.GREEN},
                new Color[]{Color.GREEN, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.GREEN},
                new Color[]{Color.GREEN, Color.BLUE, Color.BLUE, Color.GREEN, Color.GREEN, Color.GREEN, Color.BLUE, Color.BLUE, Color.GREEN},
                new Color[]{Color.GREEN, Color.BLUE, Color.BLUE, Color.GREEN, Color.GREEN, Color.GREEN, Color.BLUE, Color.BLUE, Color.GREEN},
                new Color[]{Color.GREEN, Color.BLUE, Color.BLUE, Color.GREEN, Color.GREEN, Color.GREEN, Color.BLUE, Color.BLUE, Color.GREEN},
                new Color[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN},
        });
    }

    @Override
    public void initListeners() {
        this.controller.addListener(new UiController2DActionListener() {
            @Override
            public void rasterizerChanged() {
                clearDebug();
                initRasterizer(panel.getRaster());
                redraw();
            }

            @Override
            public void modeSwitched() {
                clearDebug();
                redraw();
            }

            @Override
            public void clipPolyChanged() {
                redraw();
            }

            @Override
            public void clearRequested() {
                hardClear();
                redraw();
            }
        });

        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                initObjects(panel.getRaster());
                redraw();
            }
        });

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (!SwingUtilities.isLeftMouseButton(e))
                    return;

                if (controller.getMode() == DrawMode.LINE) {
                    start = new Point(e.getX(), e.getY());
                } else if (controller.getMode() == DrawMode.MOVE) {
                    start = new Point(e.getX(), e.getY());
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                DrawMode mode = controller.getMode();
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (mode == DrawMode.POLYLINE) {
                        start = new Point(e.getX(), e.getY());
                        if (debugPoly == null) {
                            debugPoly = new PolyLine(start, debugColor, true);
                        } else {
                            debugPoly.addPoint(start);
                        }
                    } else if (mode == DrawMode.COMPLEX_OBJECT) {
                        start = new Point(e.getX(), e.getY());
                    } else if (mode == DrawMode.REMOVE) {
                        deleteNearestPoint(e);
                    } else if (mode == DrawMode.RIGHT_TRIANGLE) {
                        if (start == null) {
                            start = new Point(e.getX(), e.getY());
                        } else {
                            objects.add(new RightTriangle(start, new Point(e.getX(), e.getY()), controller.getLineColor(), getCurrentFillColour()));
                            start = null;
                            debugLine = null;
                        }
                    } else if (mode == DrawMode.FILL) {
                        /// INFO: Tady změnít pro jiný seed fill

                        FillColor clr = getCurrentFillColour(true);

                        // seedFiller = new SeedFiller(panel.getRaster(), clr);

                        seedFiller = new SeedFillerQueue(panel.getRaster(), clr);

                        // seedFiller = new SeedFillerBorder(panel.getRaster(), clr, controller.getLineColor());

                        // seedFiller = new SeedFillerRow(panel.getRaster(), clr);

                        seedFiller.setSeed(new Point(e.getX(), e.getY()));
                    }
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    if (mode == DrawMode.POLYLINE && debugPoly != null) {
                        PolyLine obj = debugPoly.withColour(controller.getLineColor(), getCurrentFillColour());
                        objects.add(obj);
                        start = null;
                        debugLine = null;
                        debugPoly = null;
                    } else if (mode == DrawMode.COMPLEX_OBJECT && debugPolygon != null) {
                        Polygon obj = debugPolygon.withColour(controller.getLineColor(), getCurrentFillColour());
                        objects.add(obj);
                        start = null;
                        debugPolygon = null;
                    }
                }
                redraw();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() != MouseEvent.BUTTON1)
                    return;

                if (start == null)
                    return;

                if (controller.getMode() == DrawMode.LINE) {
                    Point current = new Point(e.getX(), e.getY());
                    objects.add(new Line(start, current, controller.getLineColor()));
                } else if (controller.getMode() == DrawMode.MOVE) {
                    movePoint(start, new Point(e.getX(), e.getY()));
                }
                debugLine = null;
                redraw();
            }
        });

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (!SwingUtilities.isLeftMouseButton(e))
                    return;

                if (start == null)
                    return;

                if (controller.getMode() == DrawMode.LINE ||
                        controller.getMode() == DrawMode.MOVE) {
                    Point current = new Point(e.getX(), e.getY());
                    debugLine = new Line(start, current, debugColor, true);
                    redraw();
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if (start == null)
                    return;

                Point current = new Point(e.getX(), e.getY());
                if (controller.getMode() == DrawMode.POLYLINE ||
                        controller.getMode() == DrawMode.RIGHT_TRIANGLE) {

                    debugLine = new Line(start, current, debugColor, true);
                } else if (controller.getMode() == DrawMode.COMPLEX_OBJECT) {
                    if (debugPolygon == null) {
                        debugPolygon = new Polygon(start, current, debugColor, true);
                    } else {
                        debugPolygon = debugPolygon.newEnd(current);
                    }
                }
                redraw();
            }
        });

        panel.addMouseWheelListener(e -> {
            if (debugPolygon == null)
                return;

            int toAdd = e.getWheelRotation() > 0 ? -1 : 1;
            debugPolygon = debugPolygon.addPointCount(toAdd);
            redraw();
        });

        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char key = e.getKeyChar();
                if (key == 'c') {
                    hardClear();
                } else if (key == 'm') {
                    controller.switchMode();
                } else if (key == 'r') {
                    controller.switchRasterizer();
                }
            }
        });

    }

    /**
     * Vrátí barvu na vyplnění podle aktuálního nastavení v GUI
     */
    private FillColor getCurrentFillColour() {
        return getCurrentFillColour(false);
    }

    /**
     * Vrátí barvu na vyplnění podle aktuálního nastavení v GUI
     *
     * @param mustHaveColour Použít barvu i když není vybrána barva vyplňování
     */
    private FillColor getCurrentFillColour(boolean mustHaveColour) {
        Color fillColour = controller.getFillColor();
        if (mustHaveColour && fillColour == null)
            fillColour = Color.PINK;

        if (controller.getShouldUsePattern()) {
            return new FillColor(fillColour, pattern);
        }
        return new FillColor(fillColour);
    }

    public void redraw() {
        panel.getRaster().clear();

        List<GraphicsObject> toCheck = new ArrayList<>(objects);

        if (debugLine != null)
            toCheck.add(debugLine);

        if (debugPoly != null)
            toCheck.add(debugPoly);

        if (debugPolygon != null)
            toCheck.add(debugPolygon);

        Optional<ClipPolyLine> clipPoly = controller.getClipPoly();

        List<GraphicsObject> toRender = new ArrayList<>(toCheck);

        if (clipPoly.isPresent()) {
            ClipPolyLine cp = clipPoly.get();

            toCheck.stream()
                    .filter(x -> x instanceof GraphicsPointedObject) // Ignorovat prosté úsečky
                    .map(x -> (GraphicsPointedObject) x)
                    .map(x -> Clipper.clip(cp, x)) // Ořezání
                    .filter(Objects::nonNull) // Ignorovat null objekty
                    .filter(x -> x.getPoints().size() > 2) // Pouze objekt s více jak 2 body - úsečky neořezávám
                    .forEach(x -> toRender.add(x.withColour(new Color(0xEE6611), new FillColor(new Color(0x881199)))));

            toRender.add(cp);
        }

        for (GraphicsObject obj : toRender) {
            if (obj instanceof GraphicsPointedObject && !obj.isDebug) {
                // Vyplnění
                scanLineFiller.setObject((GraphicsPointedObject) obj);
                scanLineFiller.fill();
            }

            // Obtažení/Vykreslení kraje
            rasterizer.rasterize(obj);
        }

        if (seedFiller != null) {
            seedFiller.fill();
        }

        panel.repaint();
    }

    private void clearDebug() {
        start = null;
        debugLine = null;
        debugPoly = null;
        debugPolygon = null;
    }

    private void hardClear() {
        objects.clear();
        seedFiller = null;
        clearDebug();
        panel.clear();
    }

    private void movePoint(Point start, Point end) {
        boolean incClipPoly = true;

        Point nearest = getNearestPoint(start, incClipPoly);
        if (nearest == null)
            return;

        GraphicsObject obj = getGraphicsObject(nearest, incClipPoly);
        if (obj == null)
            return;

        GraphicsObject newObj = obj.movePoint(nearest, end);

        int index = objects.indexOf(obj);
        if (index == -1)
            return;

        objects.set(index, newObj);
    }

    private void deleteNearestPoint(MouseEvent e) {
        deleteNearestPoint(new Point(e.getX(), e.getY()));
    }

    private void deleteNearestPoint(Point point) {
        boolean incClipPoly = false;

        Point nearest = getNearestPoint(point, incClipPoly);
        if (nearest == null)
            return;

        GraphicsObject obj = getGraphicsObject(nearest, incClipPoly);
        if (obj == null)
            return;

        int index = objects.indexOf(obj);
        if (index == -1)
            return;

        GraphicsObject newObj = obj.removePoint(nearest);
        if (newObj == null) {
            objects.remove(index);
            return;
        }

        objects.set(index, newObj);
    }

    /**
     * Vrátí objekt, který obsahuje danou instaci bodu
     *
     * @param point       Bod
     * @param incClipPoly Zahrnout ClipPoly?
     */
    public GraphicsObject getGraphicsObject(Point point, boolean incClipPoly) {
        if (point == null)
            return null;

        List<GraphicsObject> objs = getCalcObjects(incClipPoly);

        return objs
                .stream()
                .filter(x -> x.getPoints().contains(point))
                .findFirst()
                .orElse(null);
    }

    /**
     * Vrátí nejblížší bod
     *
     * @param point       Bod
     * @param incClipPoly Zahrnout ClipPoly?
     */
    public Point getNearestPoint(Point point, boolean incClipPoly) {
        if (point == null)
            return null;

        List<GraphicsObject> objs = getCalcObjects(incClipPoly);

        return objs
                .stream()
                .flatMap(x -> x.getPoints().stream())
                .min(Comparator.comparingDouble(one -> one.calcDistance(point)))
                .orElse(null);
    }

    private List<GraphicsObject> getCalcObjects(boolean incClipPoly) {
        List<GraphicsObject> objs = new ArrayList<>(objects);

        Optional<ClipPolyLine> clipPoly = controller.getClipPoly();
        if (incClipPoly && clipPoly.isPresent()) {
            objs.add(clipPoly.get());
        }
        return objs;
    }
}
