package cz.uhk.pgrf1.controllers;

import cz.uhk.pgrf1.raster.Raster;

public interface Controller {

    void initObjects(Raster raster);

    void initListeners();

}
