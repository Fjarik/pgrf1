package cz.uhk.pgrf1.models;

import cz.uhk.pgrf1.raster.fill.Pattern;

import java.awt.*;

public class FillColor {
    private final Color fillColour;
    private final Pattern pattern;

    public FillColor(Color fillColour) {
        this(fillColour, null);
    }

    public FillColor(Pattern pattern) {
        this(Color.YELLOW, pattern);
    }

    public FillColor(Color fillColour, Pattern pattern) {
        this.fillColour = fillColour;
        this.pattern = pattern;
    }

    public Color getColour(Point p) {
        return getColour(p.getX(), p.getY());
    }

    public Color getColour(int x, int y) {
        return pattern == null ? fillColour : pattern.getColour(x, y);
    }

    public boolean isEmpty() {
        return fillColour == null && pattern == null;
    }
}
