package cz.uhk.pgrf1.models.graphics;

import cz.uhk.pgrf1.models.Point;

import java.awt.*;
import java.util.Collections;

public class ClipPolyLine extends PolyLine {
    public ClipPolyLine(Point firstPoint, Color colour) {
        super(firstPoint, colour, true);
    }

    @Override
    public GraphicsObject removePoint(Point point) {
        // Neodstraňovat nic
        return this;
    }

    public void reverse() {
        Collections.reverse(this.points);
    }
}
