package cz.uhk.pgrf1.models.graphics;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;

import java.awt.*;
import java.util.List;

public class PolyLine extends GraphicsPointedObject {
    public PolyLine(Point firstPoint, Color colour, boolean isDebug) {
        this(firstPoint, colour, null, isDebug);
    }

    public PolyLine(Point firstPoint, Color colour, FillColor fillColour, boolean isDebug) {
        super(colour, fillColour, isDebug);
        this.points.add(firstPoint);
    }

    public PolyLine(List<Point> points, Color colour, boolean isDebug) {
        this(points, colour, null, isDebug);
    }

    public PolyLine(List<Point> points, Color colour, FillColor fillColour, boolean isDebug) {
        super(points, colour, fillColour, isDebug);
    }

    @Override
    public GraphicsObject removePoint(Point point) {
        points.remove(point);

        if (points.size() == 2) {
            // 2 body → úsečka
            return new Line(points.get(0), points.get(1), getColour());
        }
        return this;
    }

    @Override
    public GraphicsObject movePoint(Point from, Point to) {
        int index = points.indexOf(from);
        if (index == -1) {
            return this; // Bod nenalezen
        }
        points.set(index, to);
        return this;
    }

    public void addPoint(Point point) {
        points.add(point);
    }

    @Override
    public PolyLine withColour(Color lineColour, FillColor fillColour) {
        return new PolyLine(points, lineColour, fillColour, false);
    }
}
