package cz.uhk.pgrf1.models.graphics;

import cz.uhk.pgrf1.models.Point;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Úsečka je tvořená jako immutable - neměnná, při změně se tvoří nová instance
 */
public class Line extends GraphicsObject {

    private final Point start;
    private final Point end;

    public Line(Point start, Point end, Color colour) {
        this(start, end, colour, false);
    }

    public Line(Point start, Point end, Color colour, boolean isDebug) {
        super(colour, isDebug);
        this.start = start;
        this.end = end;
    }

    public Point getStart() {
        return start;
    }

    public Point getEnd() {
        return end;
    }

    @Override
    public List<Point> getPoints() {
        List<Point> points = new LinkedList<>();
        if (start != null)
            points.add(start);
        if (end != null)
            points.add(end);
        return points;
    }

    @Override
    public List<Line> getLines() {
        List<Line> lines = new LinkedList<>();
        lines.add(this);
        return lines;
    }

    @Override
    public GraphicsObject removePoint(Point point) {
        if (start == null) {
            return null; // Start je již null, při odebrání konce nezbude žádný bod
        }
        if (end == null) {
            return null; // End je již null, při odebrání startu nezbude žádný bod
        }

        if (start.cordsEquals(end)) {
            return null; // Body jsou totožné, po odebrání nezbude žádný bod
        }

        if (start == point) { // Shodná reference
            return new Line(end, end, getColour(), isDebug); // Odebrán start
        }

        if (end == point) { // Shodná reference
            return new Line(start, start, getColour(), isDebug); // Odebrán konec
        }

        if (point.cordsEquals(start)) {
            return new Line(end, end, getColour(), isDebug); // Odebrán start
        }

        return new Line(start, start, getColour(), isDebug); // Odebrán konec
    }

    @Override
    public GraphicsObject movePoint(Point from, Point to) {
        if (start == from) { // Shodná reference
            return new Line(to, end, getColour(), isDebug); // Start byl posunut
        }

        if (end == from) { // Shodná reference
            return new Line(start, to, getColour(), isDebug); // Konec byl posunut
        }

        if (from.cordsEquals(start)) {
            return new Line(to, end, getColour(), isDebug); // Start byl posunut
        }

        return new Line(start, to, getColour(), isDebug); // Konec byl posunut
    }

}
