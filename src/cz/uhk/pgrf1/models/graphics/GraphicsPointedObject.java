package cz.uhk.pgrf1.models.graphics;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public abstract class GraphicsPointedObject extends GraphicsObject {
    protected final List<Point> points;
    protected final FillColor fillColour;

    protected GraphicsPointedObject(Color lineColour, FillColor fillColour) {
        this(lineColour, fillColour, false);
    }

    protected GraphicsPointedObject(Color colour, FillColor fillColour, boolean isDebug) {
        this(new LinkedList<>(), colour, fillColour, isDebug);
    }

    protected GraphicsPointedObject(List<Point> points, Color colour, FillColor fillColour, boolean isDebug) {
        super(colour, isDebug);
        this.points = points;
        this.fillColour = fillColour;
    }

    @Override
    public List<Point> getPoints() {
        return points;
    }

    protected List<Point> getPointsWithReplacedOne(Point original, Point newPoint) {
        List<Point> newPoints = new LinkedList<>(points);

        int index = newPoints.indexOf(original);

        if (index == -1) {
            return null; // Bod nenalezen
        }
        newPoints.set(index, newPoint);
        return newPoints;
    }

    @Override
    public List<Line> getLines() {
        int size = points.size();

        List<Line> lines = new LinkedList<>();
        if (size < 1) {
            return lines;
        }

        for (int i = 0; i < size - 1; i++) {
            Point current = points.get(i);

            Point next = points.get(i + 1);

            lines.add(new Line(current, next, colour, isDebug));
        }
        lines.add(new Line(points.get(size - 1), points.get(0), colour, isDebug));

        return lines;
    }

    public FillColor getFillColour() {
        if (fillColour != null && fillColour.isEmpty()) {
            return null;
        }
        return fillColour;
    }

    public abstract GraphicsPointedObject withColour(Color lineColour, FillColor fillColour);

}
