package cz.uhk.pgrf1.models.graphics;

import cz.uhk.pgrf1.models.Point;

import java.awt.*;
import java.util.List;

public abstract class GraphicsObject {

    protected final Color colour;

    public final boolean isDebug;

    protected GraphicsObject(Color colour) {
        this(colour, false);
    }

    protected GraphicsObject(Color colour, boolean isDebug) {
        this.colour = colour;
        this.isDebug = isDebug;
    }

    public Color getColour() {
        return colour;
    }

    public abstract List<Point> getPoints();

    public abstract List<Line> getLines();

    public abstract GraphicsObject removePoint(Point point);

    public abstract GraphicsObject movePoint(Point from, Point to);
}
