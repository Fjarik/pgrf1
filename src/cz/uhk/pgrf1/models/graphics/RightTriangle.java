package cz.uhk.pgrf1.models.graphics;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;

import java.awt.*;
import java.util.List;

public class RightTriangle extends GraphicsPointedObject {
    public RightTriangle(Point start, Point end, Color colour) {
        this(start, end, colour, null);
    }

    public RightTriangle(Point start, Point end, Color lineColour, FillColor fillColour) {
        this(start, end, lineColour, fillColour, false);
    }

    public RightTriangle(Point start, Point end, Color colour, FillColor fillColour, boolean isDebug) {
        super(colour, fillColour, isDebug);
        points.add(start);
        points.add(end);
        Point third = calcThirdPoint(start, end);
        points.add(third);
    }

    private RightTriangle(List<Point> points, Color colour, FillColor fillColour, boolean isDebug) {
        super(points, colour, fillColour, isDebug);
    }

    private static Point calcThirdPoint(Point start, Point end) {
        Point center = start.getHalfPoint(end);
        double radius = center.calcDistance(end);

        int x = center.getX();

        // (x-x0)^2 + (y-y0)^2 = r^2, kde x0 a y0 je střed
        // x a x0 je pro střed stejné
        // y = +-sqrt(r^2)+y0
        // y = (+-r) + y0
        // y = -r + střed Y (při -r se vykresluje lépe)
        int y = (int) Math.round(-radius + center.getY());

        return new Point(x, y);
    }

    @Override
    public GraphicsObject removePoint(Point point) {
        // Trojúhelník má vždy 3 vrcholy, při odebrání jednoho → 2 body → úsečka
        return new Line(points.get(0), points.get(1), getColour(), isDebug);
    }

    @Override
    public GraphicsObject movePoint(Point from, Point to) {
        List<Point> newPoints = getPointsWithReplacedOne(from, to);
        if (newPoints == null) {
            return this; // Bod nenalezen
        }
        return new PolyLine(newPoints, getColour(), fillColour, isDebug); // Při změně bodu se již nejedná o pravoúhlý trojúhelník
    }

    @Override
    public GraphicsPointedObject withColour(Color lineColour, FillColor fillColour) {
        return new RightTriangle(points, lineColour, fillColour, false);
    }
}
