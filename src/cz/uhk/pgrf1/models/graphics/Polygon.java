package cz.uhk.pgrf1.models.graphics;

import cz.uhk.pgrf1.models.FillColor;
import cz.uhk.pgrf1.models.Point;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Pravidelný polygon
 */
public class Polygon extends GraphicsPointedObject {
    private final Point center;
    private final double radius;
    private final int pointsCount;
    private final double rotation;

    public Polygon(Point center, Point end, Color colour, boolean isDebug) {
        this(center, end, 3, colour, null, isDebug);
    }

    public Polygon(Point center, Point end, int pointsCount, Color colour, FillColor fillColour, boolean isDebug) {
        this(center, calcRadius(center, end), pointsCount, center.calcRotation(end), colour, fillColour, isDebug);
    }

    public Polygon(Point center, double radius, int pointsCount, double rotation, Color colour, FillColor fillColour, boolean isDebug) {
        super(colour, fillColour, isDebug);
        this.center = center;
        this.radius = radius;
        this.pointsCount = Math.max(pointsCount, 3); // count < 3 ? 3 : count
        this.rotation = rotation;
        this.points.addAll(calcPoints());
    }

    private Polygon(List<Point> points, Point center, double radius, double rotation, Color colour, FillColor fillColour, boolean isDebug) {
        super(points, colour, fillColour, isDebug);
        this.center = center;
        this.radius = radius;
        this.rotation = rotation;
        this.pointsCount = points.size();
    }

    private List<Point> calcPoints() {
        List<Point> points = new LinkedList<>();

        if (radius < 1) {
            return points;
        }

        for (int i = 1; i <= pointsCount; i++) {
            float doubleX = (float) (radius * Math.cos(2 * Math.PI * i / pointsCount + rotation) + center.getX());
            float doubleY = (float) (radius * Math.sin(2 * Math.PI * i / pointsCount + rotation) + center.getY());

            int x = Math.round(doubleX);
            int y = Math.round(doubleY);

            points.add(new Point(x, y));
        }

        return points;
    }

    @Override
    public GraphicsObject removePoint(Point point) {
        points.remove(point);
        if (points.size() == 2) {
            // 2 body → úsečka
            return new Line(points.get(0), points.get(1), getColour(), isDebug);
        }
        return new PolyLine(points, colour, fillColour, isDebug); // S odebraných bodem se již nejedná o pravidelný polygon
    }

    @Override
    public GraphicsObject movePoint(Point from, Point to) {
        List<Point> newPoints = getPointsWithReplacedOne(from, to);
        if (newPoints == null) {
            return this; // Bod nenalezen
        }
        return new PolyLine(newPoints, colour, fillColour, isDebug); // Při změne bodu se již nejedná o pravidelný polygon
    }

    public Polygon addPointCount(int toAdd) {
        return new Polygon(center, radius, pointsCount + toAdd, rotation, colour, fillColour, isDebug);
    }

    public Polygon newEnd(Point end) {
        return new Polygon(center, end, pointsCount, colour, fillColour, isDebug);
    }

    private static double calcRadius(Point start, Point end) {
        // return start.calcDistance(end);

        int dx = Math.abs(start.getX() - end.getX());
        int dy = Math.abs(start.getY() - end.getY());

        return Math.max(dx, dy);
    }

    @Override
    public Polygon withColour(Color lineColour, FillColor fillColour) {
        return new Polygon(points, center, radius, rotation, lineColour, fillColour, false);
    }
}
