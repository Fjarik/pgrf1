package cz.uhk.pgrf1.models;

public enum DrawMode {
    LINE("Line", "Press & Drag"),
    POLYLINE("Polyline", "Left click to add point. Right click to confirm."),
    RIGHT_TRIANGLE("Right triangle", "Left clicks to set hypotenuse."),
    COMPLEX_OBJECT("Polygons", "Left click to place center. Mouse wheel up/down to set points count. Righ click to confirm."),
    MOVE("Move", "Press & Drag & Move the nearest point."),
    REMOVE("Remove", "Click to delete the nearest point."),
    FILL("Seed fill", "Click to set seed and fill given object.");

    private static final DrawMode[] allValues = values();

    private final String name;

    private final String description;

    DrawMode(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public DrawMode next() {
        return allValues[(this.ordinal() + 1) % allValues.length];
    }
}
