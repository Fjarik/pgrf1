package cz.uhk.pgrf1.models;

import transforms.Point2D;

import java.awt.event.MouseEvent;

public class Point {
    private final int x;
    private final int y;

    public Point(Point2D original) {
        this(original.getX(), original.getY());
    }

    public Point(double x, double y) {
        this((float) x, (float) y);
    }

    public Point(float x, float y) {
        this(Math.round(x), Math.round(y));
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point original) {
        this(original.getX(), original.getY());
    }

    public Point(MouseEvent e) {
        this(e.getX(), e.getY());
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public double calcRotation(Point end) {
        return Math.atan2(end.getY() - this.getY(), end.getX() - this.getX());
    }

    public double calcDistance(Point end) {
        int x = this.getX() - end.getX();
        int y = this.getY() - end.getY();
        return Math.sqrt(x * x + y * y);
    }

    public Point getHalfPoint(Point end) {
        if (this.cordsEquals(end)) {
            return this;
        }
        int x = (this.getX() + end.getX()) / 2;
        int y = (this.getY() + end.getY()) / 2;
        return new Point(x, y);
    }

    public boolean cordsEquals(Point other) {
        return this.getX() == other.getX() && this.getY() == other.getY();
    }

    public Point addX() {
        return addX(1);
    }

    public Point addX(int toAdd) {
        return add(toAdd, 0);
    }

    public Point addY() {
        return addY(1);
    }

    public Point addY(int toAdd) {
        return add(0, toAdd);
    }

    public Point add(int x, int y) {
        return new Point(this.x + x, this.y + y);
    }

    public Point subtract(Point a) {
        return new Point(this.x - a.getX(), this.y - a.getY());
    }

    public Point normal() {
        return new Point(-y, x);
    }

}
