package cz.uhk.pgrf1.models;

public enum RasterizerMode {

    NORMAL("Filled lines"),
    DOTTED("Dotted lines");


    private static final RasterizerMode[] allValues = values();

    private final String description;

    RasterizerMode(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public RasterizerMode next() {
        return allValues[(this.ordinal() + 1) % allValues.length];
    }

}
